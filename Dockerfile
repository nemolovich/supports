FROM ubuntu:17.10
LABEL maintainer="bgohier<brian.gohier@capgemini.com>"

RUN apt-get update && apt-get install -y mysql-client \
  && apt-get -y clean && apt-get -y autoremove \
  && rm -rf /var/lib/apt/lists/* && rm -rf /usr/share/doc \

ENV MYSQL_ADMIN_PASSWORD="adminpassword"
ENV MYSQL_SERVER="sqlserver"
ENV MYSQL_PORT=3306
ENV MYSQL_USER_NAME="supports"
ENV MYSQL_USER_PASSWORD="password"
ENV INIT_DATABASE=false
ENV APP_CONTEXT="/"
ENV BACKUP_DIR="/root/backups"
ENV EXPORT_DIR="/root/exports"
ENV SHARED_CONF="/usr/local/etc/configs"

COPY conf/app.ini ${BACKUP_DIR}/conf/app.ini
COPY www/ ${BACKUP_DIR}/www/

RUN mkdir ${EXPORT_DIR}

COPY sql/* /root/sql/
RUN chmod +x /root/sql/*.sh

COPY run.sh /root/run.sh
COPY export_data.sh /root/export_data.sh
RUN chmod +x /root/*.sh

VOLUME /var/www/html
VOLUME ${EXPORT_DIR}
VOLUME ${SHARED_CONF}

COPY conf/app.ini ${SHARED_CONF}/app.ini

STOPSIGNAL SIGTERM

CMD ["/root/run.sh"]

