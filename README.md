# Support Platform Storage

Platform to store supports

## With standard docker

### Network

You need to create a network to connect the application to mysql container.

```
docker network create support-network
```

### Build

Build the Supports container with:

```
docker build -t capgemininantes/supports ./
```

### Run

The mysql container must be run before Supports container.

#### MYSQL

```
docker run --name supports_mysql -d -e MYSQL_ROOT_PASSWORD="AdminPassword" mysql:5.7.19
```

#### Supports

```
# docker run --name supports -d -e MYSQL_SERVER="supports_mysql" \
  -e MYSQL_ADMIN_PASSWORD="AdminPassword" -e APP_CONTEXT="/supports/" \
  -e INIT_DATABASE=true -e MYSQL_USER_PASSWORD="UserPassword" \
  -v /opt/volumes/php_configs:/usr/local/etc/configs:Z \
  -v /opt/volumes/supports/exports:/root/exports:Z \
  -v /opt/volumes/www:/var/www/html:Z \
  --network nginx-network capgemininantes/supports
```


## With docker-compose

```
docker-compose up
```

The compose configuration is the following:

* `PHP_SERVER` environment variable: **php7_server**
* Volumes:
  - PHP files directory: **/opt/volumes/www**
  - Logs directory: **/opt/volumes/nginx/logs**

## Access to web site

You can now access to your PHP site on the NGINX docker container IP on the mapped port
(default port 80). The phpinfo() page is displayed by default.

eg: http://localhost:8080
