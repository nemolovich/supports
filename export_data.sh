#!/bin/bash


USAGE="USAGE: $0 <OPTION[import|export]> <ARCHIVE_FILE>
PARAMETERS:
    OPTION:             import => import archive to application
                        export => export aplication data files
                        to archive
    ARCHIVE_FILE:       The archive to import or export
                        The archive must contains 2 folders into
                        root folder named 'data':
                            - 'files': containing application
                              supports attachments
                            - 'images': the local images for users
"

if [ $# -lt 2 ] ; then
	echo "ERROR: Missing parameter
${USAGE}"
	exit 1
fi

case "$1" in
	import)
		OPTION="import"
		;;
	export)
		OPTION="export"
		;;
	*)
		echo "ERROR: unknown option '$1'
${USAGE}"
		exit 2
		;;
esac

DATA_FILE="$2"

WWW_DIR=/var/www/html${APP_CONTEXT}
CONFIG_FILE=${SHARED_CONF}/app.ini

FILES_DIR="$(cat ${CONFIG_FILE} | grep -P "^upload_directory\s*=" | cut -d"=" -f2 | sed 's/\(^"\|"$\)//g')"
IMG_DIR="$(cat ${CONFIG_FILE} | grep -P "^local_img_directory\s*=" | cut -d"=" -f2 | sed 's/\(^"\|"$\)//g')"

TMP_DIR=$(mktemp -d)
DATA_DIR=${TMP_DIR}/data
mkdir ${DATA_DIR}


if [ "${OPTION}" = "export" ] ; then
	echo "Exporting data..."
	cp -rp ${WWW_DIR}/${FILES_DIR} ${DATA_DIR}/
	mv ${DATA_DIR}/${FILES_DIR} ${DATA_DIR}/files 2>/dev/null
	cp -rp ${WWW_DIR}/${IMG_DIR} ${DATA_DIR}/
	mv ${DATA_DIR}/${IMG_DIR} ${DATA_DIR}/images 2>/dev/null
	tar --exclude=\.gitkeep -cvzf ${EXPORT_DIR}/${DATA_FILE} -C ${TMP_DIR} data >/dev/null
	if [ $? -ne 0 ] ; then
		echo "Data export failed"
		exit 4
	fi
	echo "Data exported to ${DATA_FILE}"
else
	if [ ! -f ${EXPORT_DIR}/${DATA_FILE} ] ; then
		echo "ERROR: Cannot find data file ${DATA_FILE}"
		exit 3
	fi
	echo "Importing data from ${DATA_FILE}..."
	tar -xvzf ${EXPORT_DIR}/${DATA_FILE} -C ${TMP_DIR} >/dev/null
	if [ $? -ne 0 ] ; then
		echo "Data import failed"
		exit 5
	fi
	cp -rp ${DATA_DIR}/files/* ${WWW_DIR}/${FILES_DIR}/ 2>/dev/null
	cp -rp ${DATA_DIR}/images/* ${WWW_DIR}/${IMG_DIR}/ 2>/dev/null
	echo "Data imported"
fi

rm -rf ${TMP_DIR}

exit 0
