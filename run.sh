#!/bin/bash


WWW_DIR=/var/www/html${APP_CONTEXT}
CONFIG_FILE=${SHARED_CONF}/app.ini

if [ ! -r ${WWW_DIR} ] ; then
	mkdir -p ${WWW_DIR}
	chmod -R 777 ${WWW_DIR}
fi

if [ -z "$(ls "${WWW_DIR}")" ] ; then
	echo "Restore WWW backup files..."
	cp -rp ${BACKUP_DIR}/www/* ${WWW_DIR}/
fi

if [ -z "$(ls "${CONFIG_FILE}" 2>/dev/null)" -o -z "$(cat "${CONFIG_FILE}" 2>/dev/null)" ] ; then
	echo "Restore API config backup files..."
	cp -rp ${BACKUP_DIR}/conf/app.ini ${CONFIG_FILE}
	sed -i 's/<MYSQL_SERVER>/'"${MYSQL_SERVER}"'/g' ${CONFIG_FILE}
	sed -i 's/<MYSQL_PORT>/'"${MYSQL_PORT}"'/g' ${CONFIG_FILE}
	sed -i 's/<MYSQL_SCHEMA>/'"${MYSQL_USER_NAME}"'/g' ${CONFIG_FILE}
	sed -i 's/<MYSQL_USER>/'"${MYSQL_USER_NAME}"'/g' ${CONFIG_FILE}
	sed -i 's/<MYSQL_PASSWORD>/'"${MYSQL_USER_PASSWORD}"'/g' ${CONFIG_FILE}
fi

/root/sql/init_database.sh

wait_cmd()
{
	while [ -f /root/running ] ; do sleep 1 ; done
}

FILES_DIR="$(cat ${CONFIG_FILE} | grep -P "^upload_directory\s*=" | cut -d"=" -f2 | sed 's/\(^"\|"$\)//g')"
IMG_DIR="$(cat ${CONFIG_FILE} | grep -P "^local_img_directory\s*=" | cut -d"=" -f2 | sed 's/\(^"\|"$\)//g')"
chmod -R 777 ${WWW_DIR}/${FILES_DIR}
chmod -R 777 ${WWW_DIR}/${IMG_DIR}
touch /root/running
echo "
Supports is started"
wait_cmd &
pid="$!"
trap "echo 'Stopping Supports...';rm -f /root/running;" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
	wait
done

exit 0
