#!/bin/bash

EXPORT_GZ="gz"
EXPORT_SQL="sql"

USAGE="USAGE: $0 <OPTION[import|export]> <DATA_FILE> [,EXPORT_TYPE]
PARAMETERS:
    OPTION:             import => import file to database
                        export => export database to file
    DATA_FILE:          The file to import or export
OPTIONAL PARAMETER;:
    EXPORT_TYPE:        Export type in [${EXPORT_GZ},${EXPORT_SQL}]
"

if [ $# -lt 2 ] ; then
	echo "ERROR: Missing parameter
${USAGE}"
	exit 1
fi

EXPORT_SQL_CMD="cat"
EXPORT_GZ_CMD="gzip"
IMPORT_SQL_CMD="cat"
IMPORT_GZ_CMD="gunzip"

case "$1" in
	import)
		OPTION="import"
		;;
	export)
		OPTION="export"
		;;
	*)
		echo "ERROR: unknown option '$1'
${USAGE}"
		exit 2
		;;
esac

DATA_FILE="$2"

case "$3" in
	${EXPORT_SQL})
		if [ "${OPTION}" = "import" ] ; then
			ACTION_CMD=${IMPORT_SQL_CMD}
		else
			ACTION_CMD=${EXPORT_SQL_CMD}
		fi
		;;
	${EXPORT_GZ})
		if [ "${OPTION}" = "import" ] ; then
			ACTION_CMD=${IMPORT_GZ_CMD}
		else
			ACTION_CMD=${EXPORT_GZ_CMD}
		fi
		;;
	*)
		if [ "${OPTION}" = "import" ] ; then
			ACTION_CMD=${IMPORT_SQL_CMD}
		else
			ACTION_CMD=${EXPORT_SQL_CMD}
		fi
		;;
esac

if [ "${OPTION}" = "import" ] ; then
	if [ ! -f ${EXPORT_DIR}/${DATA_FILE} ] ; then
		echo "ERROR: Cannot find dump file ${DATA_FILE}"
		exit 3
	fi
	${ACTION_CMD} < ${EXPORT_DIR}/${DATA_FILE} | mysql -h ${MYSQL_SERVER} -P ${MYSQL_PORT} -u${MYSQL_USER_NAME} -p${MYSQL_USER_PASSWORD} -D ${MYSQL_USER_NAME}
	if [ $? -eq 0 ] ; then
		echo "Dump imported from ${DATA_FILE}"
	else
		echo "Dump import failed"
		exit 4
	fi
else
	mysqldump -h ${MYSQL_SERVER} -P ${MYSQL_PORT} -u${MYSQL_USER_NAME} -p${MYSQL_USER_PASSWORD} --databases ${MYSQL_USER_NAME} | ${ACTION_CMD} > ${EXPORT_DIR}/${DATA_FILE}
	if [ $? -eq 0 ] ; then
		echo "Dump exported to ${DATA_FILE}"
	else
		echo "Dump export failed"
		exit 5
	fi
fi

exit 0
