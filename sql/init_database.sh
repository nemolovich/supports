#!/bin/bash

ret=0

FLAG=/root/sql/intialized

NEED_INIT=false
if [ "${INIT_DATABASE}" = "true" -a ! -f ${FLAG} ] ; then
	NEED_INIT=true
elif [ "${INIT_DATABASE}" != "true" ] ; then
	touch ${FLAG}
fi

[ "$1" = "init" ] && INIT=true || INIT=false
[ "$1" = "reset" ] && RESET=true || RESET=false
[ "$1" = "drop" ] && DROP=true || DROP=false
[ "$1" = "delete" ] && DELETE=true || DELETE=false

init_database()
{
	echo "Initialize schema '${MYSQL_USER_NAME}' on ${MYSQL_SERVER}:${MYSQL_PORT}..."
	mysql -h ${MYSQL_SERVER} -P ${MYSQL_PORT} -uroot -p${MYSQL_ADMIN_PASSWORD} <<EOF
create user ${MYSQL_USER_NAME} identified by '${MYSQL_USER_PASSWORD}';
create schema ${MYSQL_USER_NAME};
grant all privileges on ${MYSQL_USER_NAME}.* to ${MYSQL_USER_NAME};

EOF

	ret=$?
	if [ ${ret} -eq 0 ] ; then
		echo "Schema '${MYSQL_USER_NAME}' initialized"
	else
		echo "Schema '${MYSQL_USER_NAME}' initialization failed"
	fi
	return ${ret}
}

delete_database()
{
	echo "Delete schema '${MYSQL_USER_NAME}' on ${MYSQL_SERVER}:${MYSQL_PORT}..."
	mysql -h ${MYSQL_SERVER} -P ${MYSQL_PORT} -uroot -p${MYSQL_ADMIN_PASSWORD} <<EOF
drop schema ${MYSQL_USER_NAME};
drop user ${MYSQL_USER_NAME};

EOF

	ret=$?
	if [ ${ret} -eq 0 ] ; then
		echo "Schema '${MYSQL_USER_NAME}' deleted"
	else
		echo "Schema '${MYSQL_USER_NAME}' deletion failed"
	fi
	return ${ret}
}

create_tables()
{
	echo "Create tables..."
	mysql -h ${MYSQL_SERVER} -P ${MYSQL_PORT} -u${MYSQL_USER_NAME} -p${MYSQL_USER_PASSWORD} -D ${MYSQL_USER_NAME} < /root/sql/create.sql
	ret=$?
	if [ ${ret} -eq 0 ] ; then
		echo "Create tables done"
	else
		echo "Create tables failed"
	fi
	return ${ret}
}

drop_tables()
{
	echo "Drop tables..."
	mysql -h ${MYSQL_SERVER} -P ${MYSQL_PORT} -u${MYSQL_USER_NAME} -p${MYSQL_USER_PASSWORD} -D ${MYSQL_USER_NAME} < /root/sql/drop.sql
	ret=$?
	if [ ${ret} -eq 0 ] ; then
		echo "Drop tables done"
	else
		echo "Drop tables failed"
	fi
	return ${ret}
}

if ${NEED_INIT} || ${INIT} ; then
	echo "Init starting..."
	init_database
	ret=$?
	create_tables
	req=$?
	ret=$((ret+req))
	[ ${ret} -eq 0 ] &&	touch ${FLAG}
elif ${RESET} ; then
	echo "Reset starting..."
	drop_tables
	ret=$?
	create_tables
	req=$?
	ret=$((ret+req))
elif ${DROP} ; then
	echo "Drop starting..."
	drop_tables
	ret=$?
elif ${DELETE} ; then
	echo "Delete starting..."
	delete_database
	ret=$?
fi

exit ${ret}
