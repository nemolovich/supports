<?php

	$supports = get_all_supports();

	uasort($supports, function($a, $b) {
		return get_int_date($b->date) - get_int_date($a->date);
	});
	
	$dates = array_unique(array_values(array_map(function($o) {
		return get_month_date($o->date);
	}, $supports)));
	ksort($dates);
	
	$now = time();
	$content = Array();
	
	foreach ($supports as $support) {
		$f_date = get_month_date($support->date);
		if (!array_key_exists($f_date, $content)) {
			$content[$f_date] = [];
		}
		// $support->date = $f_date;
		$content[$f_date][] = $support;
		// }
	}
	
	foreach ($content as $k => $supp) {
		uasort($supp, function($a, $b) {
			$d_a = get_int_date($a->date);
			$d_b = get_int_date($b->date);
			$n_a = strcmp($a->name, $b->name);
			return $d_a != $d_b ? $d_b - $d_a : $n_a;
		});
		$content[$k] = $supp;
	}
	
	$content = sizeof($content) > MAX_DATES ? array_slice($content, 0, MAX_DATES) : $content;
	$dates = sizeof($dates) > MAX_DATES ? array_slice($dates, 0, MAX_DATES) : $dates;

	$is_index = True;
?>
