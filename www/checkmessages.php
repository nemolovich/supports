<?php
use utils\LDAP\LDAPConnection as LDAPConnection;
use utils\Messages\Message as Message;
use utils\Messages\MessageSeverity as MessageSeverity;
use const utils\Messages\MSG_ATTR;
use function utils\Messages\get_messages;
use function utils\Messages\has_next;
use function utils\Messages\next_message;

if (isset($_SESSION[MSG_ATTR])) {
	$messages = get_messages();
	if (has_next($messages)) {
		echo '<div class="alert-container">';
		while (has_next($messages)) {
			$message = next_message($messages);
			$m = $message;
			echo '<div class="chip z-depth-3 alert alert-'.($message->severity).'">
				<i class="close material-icons">close</i>
				'.preg_replace('/\n/', '<br/>', $message->msg).'
				</div>';
			$message = null;
		}
		echo '</div>';
	}
	unset($_SESSION[MSG_ATTR]);
	unset($messages);
	$_SESSION[MSG_ATTR] = null;
}
?>