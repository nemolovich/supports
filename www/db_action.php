<?php
	// session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			include_once './functions.php';

			if (check_admin()) {

				if (!isset($_POST['php_action'])) {
					$messages[] = new utils\Messages\Message('Invalid PHP request', utils\Messages\MessageSeverity::ERROR);
				} else {

					switch ($_POST['php_action']) {
						case 'delete_support':
							if (isset($_POST['support_id'])) {
								$support = get_support($_POST['support_id']);
								if ($support !== null && remove_support($support) && delete_support($support->id)) {
									$messages[] = new utils\Messages\Message('Support successfully removed', utils\Messages\MessageSeverity::SUCCESS);
								} else {
									$messages[] = new utils\Messages\Message('Error while deleting support', utils\Messages\MessageSeverity::ERROR);
								}
							} else {
								$messages[] = new utils\Messages\Message('Invalid support ID', utils\Messages\MessageSeverity::ERROR);
							}
							break;
						default:
							$messages[] = new utils\Messages\Message('Unknown action', utils\Messages\MessageSeverity::ERROR);
							break;
					}

				}
				set_session_messages($messages);
			}
			echo "<meta http-equiv='refresh' content='0; url=".APP_CONTEXT."'>";
		?>
	</head>
	<body>
	</body>
</html>
