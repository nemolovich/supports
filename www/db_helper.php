<?php

	function get_all_supports() {
		$supports = [];
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
		$result = $conn->query("SELECT * FROM SUPPORT");
		if ($result) {
			while($row = $result->fetch_array())
			{
				$supp_id = $row['SUPPORT_ID'];
				$prez_date = $row['SUPPORT_DATE'];
				if (ENCODING != DEFAULT_ENCODING) {
					$name = iconv(ENCODING, DEFAULT_ENCODING, $row['SUPPORT_NAME']);
					$auth = iconv(ENCODING, DEFAULT_ENCODING, $row['SUPPORT_AUTH']);
					$sender = iconv(ENCODING, DEFAULT_ENCODING, $row['SUPPORT_SENDER']);
				} else {
					$name = $row['SUPPORT_NAME'];
					$auth = $row['SUPPORT_AUTH'];
					$sender = $row['SUPPORT_SENDER'];
				}
				$supp = new Support($name);
				$supp->id = $supp_id;
				$supp->auth = $auth;
				$supp->sender = $sender;
				// $supp->date = get_expiration_date($prez_date);
				$supp->date = $prez_date;
				$att_req = $conn->query("SELECT * FROM ATTACHMENT where SUPPORT_ID='$supp_id'");
				if ($att_req) {
					while($att_row = $att_req->fetch_array())
					{
						$att_id = $att_row['ATTACHMENT_ID'];
						if (ENCODING != DEFAULT_ENCODING) {
							$type = iconv(ENCODING, DEFAULT_ENCODING, $att_row['ATTACHMENT_TYPE']);
							$path = iconv(ENCODING, DEFAULT_ENCODING, $att_row['ATTACHMENT_PATH']);
						} else {
							$type = $att_row['ATTACHMENT_TYPE'];
							$path = $att_row['ATTACHMENT_PATH'];
						}
						$att = new Attachment($type, $path);
						$att->id = $att_id;
						$supp->add_attachment($att);
					}
					$att_req->close();
				}
				$supports[] = $supp;
			}
			$result->close();
		}
		$conn->close();
		return $supports;
	}

	function get_support($id) {
		$support = null;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
		$result = $conn->query("SELECT * FROM SUPPORT");
		if ($result) {
			while($row = $result->fetch_array())
			{
				$supp_id = $row['SUPPORT_ID'];
				if ($supp_id == $id) {
					$prez_date = $row['SUPPORT_DATE'];
					if (ENCODING != DEFAULT_ENCODING) {
						$name = iconv(ENCODING, DEFAULT_ENCODING, $row['SUPPORT_NAME']);
						$auth = iconv(ENCODING, DEFAULT_ENCODING, $row['SUPPORT_AUTH']);
						$sender = iconv(ENCODING, DEFAULT_ENCODING, $row['SUPPORT_SENDER']);
					} else {
						$name = $row['SUPPORT_NAME'];
						$auth = $row['SUPPORT_AUTH'];
						$sender = $row['SUPPORT_SENDER'];
					}
					$supp = new Support($name);
					$supp->id = $supp_id;
					$supp->auth = $auth;
					$supp->sender = $sender;
					// $supp->date = get_expiration_date($prez_date);
					$supp->date = $prez_date;
					$att_req = $conn->query("SELECT * FROM ATTACHMENT where SUPPORT_ID='$supp_id'");
					if ($att_req) {
						while($att_row = $att_req->fetch_array())
						{
							$att_id = $att_row['ATTACHMENT_ID'];
							if (ENCODING != DEFAULT_ENCODING) {
								$type = iconv(ENCODING, DEFAULT_ENCODING, $att_row['ATTACHMENT_TYPE']);
								$path = iconv(ENCODING, DEFAULT_ENCODING, $att_row['ATTACHMENT_PATH']);
							} else {
								$type = $att_row['ATTACHMENT_TYPE'];
								$path = $att_row['ATTACHMENT_PATH'];
							}
							$att = new Attachment($type, $path);
							$att->id = $att_id;
							$supp->add_attachment($att);
						}
						$att_req->close();
					}
					$support = $supp;
					break;
				}
			}
			$result->close();
		}
		$conn->close();
		return $support;
	}

	function support_exists($supp_name) {
		$exists = False;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
		$result = $conn->query("SELECT * FROM SUPPORT");
		if ($result) {
			while($row = $result->fetch_array())
			{
				$name = $row['SUPPORT_NAME'];
				if ($name == $supp_name) {
					$exists = True;
					break;
				}
			}
			$result->close();
		}
		$conn->close();
		return $exists;
	}

	function attachment_exists($supp_id, $att_type) {
		$exists = False;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
		$result = $conn->query("SELECT * FROM ATTACHMENT WHERE SUPPORT_ID=$supp_id");
		if ($result) {
			while($row = $result->fetch_array())
			{
				$type = $row['ATTACHMENT_TYPE'];
				if ($type === $att_type) {
					$exists = True;
					break;
				}
			}
			$result->close();
		}
		$conn->close();
		return $exists;
	}

	function insert_support($supp) {
		global $messages;
		$inserted = False;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);
		$conn->autocommit(FALSE);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
		$req = sprintf("INSERT INTO SUPPORT (SUPPORT_NAME, SUPPORT_DATE, SUPPORT_AUTH, SUPPORT_SENDER) VALUES ('%s', '%s', '%s', '%s');",
				mysqli_real_escape_string($conn, $supp->name), $supp->date,
				mysqli_real_escape_string($conn, $supp->auth),
				mysqli_real_escape_string($conn, $supp->sender));
		var_dump($req);
		$inserted = $conn->query($req);
		if (!$inserted) {
			if (ENCODING != DEFAULT_ENCODING) {
				$errmsg = iconv(ENCODING, DEFAULT_ENCODING, $conn->error);
			} else {
				$errmsg = $conn->error;
			}
			$errcode = $conn->errno;
			$messages[] = new utils\Messages\Message(sprintf("SQL Support Error [%d]: %s", $errcode, $errmsg),
				utils\Messages\MessageSeverity::ERROR);
		}
		$supp_id = $conn->insert_id;
		foreach ($supp->attachments as $att) {
			$req = sprintf("INSERT INTO ATTACHMENT (SUPPORT_ID, ATTACHMENT_PATH, ATTACHMENT_TYPE) VALUES (%d, '%s', '%s');",
					$supp_id, mysqli_real_escape_string($conn, $att->path), mysqli_real_escape_string($conn, $att->type));
			$inserted = $conn->query($req);
			if (!$inserted) {
				if (ENCODING != DEFAULT_ENCODING) {
					$errmsg = iconv(ENCODING, DEFAULT_ENCODING, $conn->error);
				} else {
					$errmsg = $conn->error;
				}
				$errcode = $conn->errno;
				$messages[] = new utils\Messages\Message(sprintf("SQL Attachment Error [%d]: %s", $errcode, $errmsg),
					utils\Messages\MessageSeverity::ERROR);
				break;
			}
		}
		if ($inserted) {
			$conn->commit();
		} else {
			$conn->rollback();
		}
		$conn->close();
		return $inserted;
	}

	function delete_support($supp_id) {
		global $messages;
		$removed = False;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
	
		$removed = $conn->query("DELETE FROM SUPPORT where SUPPORT_ID='$supp_id'");
		if (!$removed) {
			if (ENCODING != DEFAULT_ENCODING) {
				$errmsg = iconv(ENCODING, DEFAULT_ENCODING, $conn->error);
			} else {
				$errmsg = $conn->error;
			}
			$errcode = $conn->errno;
			$messages[] = new utils\Messages\Message(sprintf("SQL Support Error [%d]: %s", $errcode, $errmsg),
				utils\Messages\MessageSeverity::ERROR);
		}
		$conn->close();
		return $removed;
	}

	function delete_attachment($att_id) {
		global $messages;
		$removed = False;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
	
		$removed = $conn->query("DELETE FROM ATTACHMENT where ATTACHMENT_ID='$att_id'");
		if (!$removed) {
			if (ENCODING != DEFAULT_ENCODING) {
				$errmsg = iconv(ENCODING, DEFAULT_ENCODING, $conn->error);
			} else {
				$errmsg = $conn->error;
			}
			$errcode = $conn->errno;
			$messages[] = new utils\Messages\Message(sprintf("SQL Attachment Error [%d]: %s", $errcode, $errmsg),
				utils\Messages\MessageSeverity::ERROR);
		}
		$conn->close();
		return $removed;
	}

	function update_support($supp) {
		global $messages;
		$updated = False;
		$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD);
		$conn->autocommit(FALSE);

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$conn->select_db(DB_SCHEMA);
		$supp_id = $supp->id;
		$req = sprintf("UPDATE SUPPORT SET SUPPORT_NAME='%s', SUPPORT_DATE='%s', SUPPORT_SENDER='%s', SUPPORT_AUTH='%s' WHERE SUPPORT_ID=%s;",
				mysqli_real_escape_string($conn, $supp->name), $supp->date,
				mysqli_real_escape_string($conn, $supp->sender),
				mysqli_real_escape_string($conn, $supp->auth),
				$supp_id);
		$updated = $conn->query($req);
		if (!$updated) {
			if (ENCODING != DEFAULT_ENCODING) {
				$errmsg = iconv(ENCODING, DEFAULT_ENCODING, $conn->error);
			} else {
				$errmsg = $conn->error;
			}
			$errcode = $conn->errno;
			$messages[] = new utils\Messages\Message(sprintf("SQL Support Error [%d]: %s", $errcode, $errmsg),
				utils\Messages\MessageSeverity::ERROR);
		}
		foreach ($supp->attachments as $att) {
			if (attachment_exists($supp_id, $att->type)) {
				$req = sprintf("UPDATE ATTACHMENT SET ATTACHMENT_PATH='%s', ATTACHMENT_TYPE='%s' WHERE ATTACHMENT_ID='%s';",
						mysqli_real_escape_string($conn, $att->path),
						mysqli_real_escape_string($conn, $att->type), $att->id);
			} else {
				$req = sprintf("INSERT INTO ATTACHMENT (SUPPORT_ID, ATTACHMENT_PATH, ATTACHMENT_TYPE) VALUES (%d, '%s', '%s');",
						$supp_id, mysqli_real_escape_string($conn, $att->path),
						mysqli_real_escape_string($conn, $att->type));
			}
			$updated = $conn->query($req);
			if (!$updated) {
				if (ENCODING != DEFAULT_ENCODING) {
					$errmsg = iconv(ENCODING, DEFAULT_ENCODING, $conn->error);
				} else {
					$errmsg = $conn->error;
				}
				$errcode = $conn->errno;
				$messages[] = new utils\Messages\Message(sprintf("SQL Attachment Error [%d]: %s", $errcode, $errmsg),
					utils\Messages\MessageSeverity::ERROR);
				break;
			}
		}
		if ($updated) {
			$conn->commit();
		} else {
			$conn->rollback();
		}
		$conn->close();
		return $updated;
	}
?>
