<?php
	// session_start();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<?php include_once "./header.php"; ?>
		<title><?=APP_TITLE?> - Edit a support</title>
	</head>
	<body>

		<?php include_once "./navbar.php"; ?>
		<?php include_once "./footer.php"; ?>
		<main>
			<?php include_once "./checkmessages.php"; ?>
			<?php 
				if (!check_dev()) {
					die('Access denied');
				}
				if (!isset($_POST['edit_support_id']) && !isset($_SESSION['edit_support_id'])) {
					$messages[] = new utils\Messages\Message('Invalid Support request',
						utils\Messages\MessageSeverity::ERROR);
					set_session_messages(array_merge($ldap_con->get_messages(), $messages));
					echo "<meta http-equiv='refresh' content='0; url=".APP_CONTEXT."'>";
					die('Invalid Support');
				}

				$support = get_support(isset($_POST['edit_support_id']) ? $_POST['edit_support_id'] : $_SESSION['edit_support_id']);
				// var_dump($support);
				$_SESSION['edit_support_id'] = $support->id;
				if (!$support) {
					$messages[] = new utils\Messages\Message('Support could not be found',
						utils\Messages\MessageSeverity::ERROR);
					set_session_messages(array_merge($ldap_con->get_messages(), $messages));
					echo "<meta http-equiv='refresh' content='0; url=".APP_CONTEXT."'>";
					die('Cannot find support');
				}
				$att_ppt = null;
				$att_record = null;
				$att_sources = null;
				$att_ppt_path = null;
				$att_record_path = null;
				$att_sources_path = null;

				foreach ($support->attachments as $att) {
					if ($att->type === PPT_TYPE) {
						$att_ppt = $att;
						$att_ppt_path = $att->path;
					} else if ($att->type === RECORD_TYPE) {
						$att_record = $att;
						$att_record_path = $att->path;
					} else if ($att->type === SOURCES_TYPE) {
						$att_sources = $att;
						$att_sources_path = $att->path;
					}
				}

				$support_name = '';
				$enc_support_name = $support->name;
				$support_auth = '';
				$enc_support_auth = $support->auth;
				$support_date = $support->date;

				$edit_allowed = (is_admin() || is_author($support));
				if (isset($_POST['supportName'])) {
					$enc_support_name = $_POST['supportName'];
					$enc_support_auth = $_POST['supportAuth'];
					if (ENCODING != DEFAULT_ENCODING) {
						$support_name = iconv(DEFAULT_ENCODING, ENCODING, $enc_support_name);
						$support_auth = iconv(DEFAULT_ENCODING, ENCODING, $enc_support_auth);
					} else {
						$support_name = $enc_support_name;
						$support_auth = $enc_support_auth;
					}
					$support_date = (isset($_POST['supportDate']) ? $_POST['supportDate'] : $support_date);

					$support->name = $support_name;
					$support->auth = $support_auth;
					$support->date = $support_date;

					if ($_POST['supportsources-del'] == 'true') {
						$att_sources = null;
					}
					if ($_POST['supportvideo-del'] == 'true') {
						$att_record = null;
					}

					foreach ($_FILES as $f_type => $f_infos) {
						if ($f_type == 'supportppt' && !empty($f_infos['name'])) {
							$a_type = PPT_TYPE;
							$att_ppt = treat_file($f_infos, $a_type);
							if (ENCODING != DEFAULT_ENCODING) {
								$att_ppt_path = iconv(ENCODING, DEFAULT_ENCODING, $att_ppt->path);
							} else {
								$att_ppt_path = $att_ppt->path;
							}
						} elseif ($f_type == 'supportvideo' && !empty($f_infos['name'])) {
							$a_type = RECORD_TYPE;
							$att_record = treat_file($f_infos, $a_type);
							if (ENCODING != DEFAULT_ENCODING) {
								$att_record_path = iconv(ENCODING, DEFAULT_ENCODING, $att_record->path);
							} else {
								$att_record_path = $att_record->path;
							}
							if (!$support->has_attachment(RECORD_TYPE)) {
								$support->add_attachment($att_record);
							}
						} elseif ($f_type == 'supportsources' && !empty($f_infos['name'][0])) {
							$a_type = SOURCES_TYPE;
							if (sizeof($f_infos['name']) > 1) {
								$zipdir = UPLOAD_DIR.'/'.$support_name.'.zip';
								$zippath = './'.$zipdir;
								if (ENCODING != DEFAULT_ENCODING) {
									$enc_zippath = iconv(ENCODING, DEFAULT_ENCODING, $zipdir);
								} else {
									$enc_zippath = $zipdir;
								}
								$zip = new ZipArchive;
								$zip->open($enc_zippath, ZipArchive::CREATE);
								for ($i=0; $i<sizeof($f_infos['name']); $i++) {
									$file_name = $f_infos['name'][$i];
									$file_name = $file_name;
									$file_type = $f_infos['type'][$i];
									$file_path = $f_infos['tmp_name'][$i];
									$file_error = $f_infos['error'][$i];
									$file_size = $f_infos['size'][$i];
									if (file_exists($file_path)) {
										$zip->addFile($file_path, $file_name);
									} else {
										$messages[] = new utils\Messages\Message(sprintf("Cannot locate file '%s' as '%s'", $file_name, $file_path),
											utils\Messages\MessageSeverity::WARNING);
									}
								}
								$zip->close();
								$att_sources = new Attachment($a_type, $zipdir);
								if (ENCODING != DEFAULT_ENCODING) {
									$att_sources_path = iconv(ENCODING, DEFAULT_ENCODING, $att_sources->path);
								} else {
									$att_sources_path = $att_sources->path;
								}
								if (!$support->has_attachment(SOURCES_TYPE)) {
									$support->add_attachment($att_sources);
								}
							} else {
								if (!empty($f_infos['name'][0])) {
									if (ENCODING != DEFAULT_ENCODING) {
										$file_name = iconv(DEFAULT_ENCODING, ENCODING, $f_infos['name'][0]);
									} else {
										$file_name = $f_infos['name'][0];
									}
									$file_dir = UPLOAD_DIR.'/'.$file_name;
									$file_type = $f_infos['type'][0];
									$file_path = $f_infos['tmp_name'][0];
									$file_error = $f_infos['error'][0];
									$file_size = $f_infos['size'][0];
									$upload_file = './'.$file_dir;
									if (upload_file($file_path, $file_name, $upload_file)) {
										$att_sources = new Attachment($a_type, $file_dir);
										if (ENCODING != DEFAULT_ENCODING) {
											$att_sources_path = iconv(ENCODING, DEFAULT_ENCODING, $att_sources->path);
										} else {
											$att_sources_path = $att_sources->path;
										}
										if (!$support->has_attachment(SOURCES_TYPE)) {
											$support->add_attachment($att_sources);
										}
									}
								}
							}
						}
					}

					foreach ($support->attachments as $att) {
						if ($att->type === PPT_TYPE) {
							if (ENCODING != DEFAULT_ENCODING) {
								$enc_path = iconv(ENCODING, DEFAULT_ENCODING, $att_ppt->path);
							} else {
								$enc_path = $att_ppt->path;
							}
							if ($att->path !== $enc_path && $att->path !== $att_ppt->path && !remove_file($att->path)) {
								$messages[] = new utils\Messages\Message(sprintf("Cannot remove file '%s'", $att->path),
									utils\Messages\MessageSeverity::WARNING);
							}
							$support->update_attachment($att_ppt);
						} elseif ($att->type === RECORD_TYPE) {
							if ($att_record !== null) {
								if (ENCODING != DEFAULT_ENCODING) {
									$enc_path = iconv(ENCODING, DEFAULT_ENCODING, $att_record->path);
								} else {
									$enc_path = $att_record->path;
								}
								$att_path = $att_record->path;
							} else {
								$enc_path = '';
								$att_path = null;
							}
							if ($att->path !== $enc_path && $att->path !== $att_path) {
								if (remove_file($att->path)) {
									if ($att_record === null) {
										$support->remove_attachment($att);
										delete_attachment($att->id);
									}
								} else {
									$messages[] = new utils\Messages\Message(sprintf("Cannot remove file '%s'", $att->path),
										utils\Messages\MessageSeverity::WARNING);
								}
							}
							$support->update_attachment($att_record);
						} elseif ($att->type === SOURCES_TYPE) {
							if ($att_sources !== null) {
								if (ENCODING != DEFAULT_ENCODING) {
									$enc_path = iconv(ENCODING, DEFAULT_ENCODING, $att_sources->path);
								} else {
									$enc_path = $att_sources->path;
								}
								$att_path = $att_sources->path;
							} else {
								$enc_path = '';
								$att_path = null;
							}
							if ($att->path !== $enc_path && $att->path !== $att_path) {
								if (remove_file($att->path)) {
									if ($att_sources === null) {
										$support->remove_attachment($att);
										delete_attachment($att->id);
									}
								} else {
									$messages[] = new utils\Messages\Message(sprintf("Cannot remove file '%s'", $att->path),
										utils\Messages\MessageSeverity::WARNING);
								}
							}
							$support->update_attachment($att_sources);
						}
					}

					if (update_support($support)) {
						$messages[] = new utils\Messages\Message(sprintf("Support '%s' successfully updated", $enc_support_name),
							utils\Messages\MessageSeverity::SUCCESS);
					}
					set_session_messages($messages);

					echo "<meta http-equiv='refresh' content='0'>";
				}

			?>
			<div class="container">
				<?php include_once './loader.php'; ?>
				<div class="row content">
					<form enctype="multipart/form-data" action="<?php print $_SERVER['PHP_SELF']; ?>"
						  method="post" name="editForm" class="col s12">

						<input name="edit_support_id" type="hidden"
							   value="<?=$support->id?>">
						<h3 class="form-signin-heading">Edit a support</h3>

						<div class="row">
							<div class="col s12">
								<div class="input-field">
									<input id="support-name" name="supportName" type="text" maxlength="100"
										   class="validate <?=(!$edit_allowed)?'disabled':''?>" value="<?=$enc_support_name?>"
										   autofocus="" required="" data-length="100"
										   <?=$edit_allowed?'':'readonly=""'?>>
									<label for="support-name">Name for this support</label>
								<div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="input-field">
									<input id="support-auth" name="supportAuth" type="text" maxlength="100"
										   class="validate <?=(!$edit_allowed)?'disabled':''?>" value="<?=$enc_support_auth?>"
										   required="" data-length="100"
										   <?=$edit_allowed?'':'readonly=""'?>>
									<label for="support-auth">Presentation author(s)</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="input-field">
									<input id="support-date" name="supportDate" type="date"
										   class="datepicker validate <?=(!$edit_allowed)?'disabled':''?>" required=""
										   pattern="20[0-9]{2}-(0[0-9]|1[0-2])-([0-2][0-9]|3[01])"
										   value="<?=$support_date?>"
										   <?=$edit_allowed?'':'disabled=""'?>>
									<label for="support-date">Presentation date</label>
								</div>
							</div>
						</div>
						<div class="input-field file-field">
							<div class="btn btn-small large <?=(!$edit_allowed)?'disabled':''?>">
								<span>Change the PowerPoint</span>
								<input type="file" name="supportppt" accept=".ppt, .pptx"
									   class="confirm-action" att-type="powerpoint" <?=$edit_allowed?'':'readonly=""'?>>
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="Modify presentation"
									   value="<?=basename($att_ppt_path)?>" disabled readonly>
							</div>
						</div>
						<div class="input-field file-field">
							<?php if ($edit_allowed&&($att_record)): ?>
								<button class="del-action red btn btn-small btn-floating" type="button"
										del-field="supportvideo-del" title="Delete record">
									<i class="material-icons">delete</i>
								</button>
							<?php endif ?>
							<div class="btn btn-small large <?=(!$edit_allowed&&($att_record))?'disabled':''?>">
								<span><?=($att_record)?'Change the':'Browse a'?> record</span>
								<input type="file" name="supportvideo" accept="video/*"
									   class="confirm-action" att-type="record" <?=(!$edit_allowed&&($att_record))?'readonly=""':''?>>
								<input type="hidden" name="supportvideo-del" value="false">
								<!--input type="file" name="supportvideo"-->
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="<?=($att_record)?'Modify':'Upload'?> video"
									   value="<?=($att_record)?basename($att_record_path):''?>" disabled readonly>
							</div>
						</div>
						<div class="input-field file-field">
							<?php if ($edit_allowed&&($att_sources)): ?>
								<button class="del-action red btn btn-small btn-floating" type="button"
										del-field="supportsources-del" title="Delete sources">
									<i class="material-icons">delete</i>
								</button>
							<?php endif ?>
							<div class="btn btn-small large <?=(!$edit_allowed&&($att_sources))?'disabled':''?>">
								<span><?=($att_sources)?'Change':'Browse'?> sources</span>
								<input type="file" name="supportsources[]" multiple
									   class="confirm-action" att-type="sources"  <?=(!$edit_allowed&&($att_sources))?'readonly':''?>>
								<input type="hidden" name="supportsources-del" value="false">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="<?=($att_sources)?'Modify the':'Upload multiple'?> sources files"
									   value="<?=($att_sources)?basename($att_sources_path):''?>" disabled readonly>
							</div>
						</div>
						<input type="hidden" name="MAX_FILE_SIZE" value="1000000000">
						<button class="btn btn-large blue" name="Submit" value="Upload"
								type="submit">
							Submit
						</button>
						<button class="btn grey" name="Cancel" value="Cancel" type="reset"
							onclick="forms['editForm'].reset();location.href='<?=APP_CONTEXT?>./reset.php';">
							Cancel
						</button>
					</form>
				</div>
			</div>
			<div class="col s12 clear-bottom">
			</div>
		</main>
	</body>
	<script type="text/javascript">

		$(document).ready(function() {
			$('.confirm-action').click(function() {
				var elm = $(this);
				var value_input = elm.parent().parent().find('.file-path-wrapper>.file-path');
				return value_input.val().length<1 || confirm('Do you really want to remove current ' + elm.attr('att-type') + ' attachment ?');
			});
			$('.del-action').click(function() {
				var elm = $(this);
				var del_elm_name = elm.attr('del-field');
				var del_input = elm.parent().find('input[type="hidden"][name="' + del_elm_name + '"]');
				var file_input = elm.parent().find('input.file-path');
				var confirm_button = elm.parent().find('input.confirm-action');
				var deleted = false;
				if (confirm('Do you really want to remove current ' + confirm_button.attr('att-type') + ' attachment ?')) {
					del_input.val('true');
					file_input.val('');
					elm.remove();
					deleted = true;
				}
				return deleted;
			});
		});
	</script>
</html>
