
	<a id="footer-details" class="waves-effect waves-light waves-teal light-blue lighten-1 btn btn-floating"
	   onclick="$('#footer-target').tapTarget('open');">
		<i class="material-icons">lightbulb_outline</i>
	</a>

	<div class="tap-target light-blue ligthen-1 white-text" id="footer-target" data-activates="footer-details">
		<div class="tap-target-content">
			<div class="content">
				<h5><?=APP_TITLE?></h5>
				<i>Developped by&nbsp;&nbsp;</i>
				<b><a class="grey-text text-darken-3"
					  href="mailto:brian.gohier@capgemini.com?subject=<?=APP_TITLE?> Application"
					  title="Send mail to Brian GOHIER">
					Brian GOHIER
				</a></b>
				<br>
				<i>Under </i>
				<b><a href="http://www.gnu.org/licenses/gpl.html"
						 class="grey-text text-darken-3" target="_blank"
						 title="GNU GPLv3 Lincense">
					GPU GPLv3 License
				</a></b>
				<a class="gitlab-link" target="_blank"
					href="https://gitlab.com/nemolovich/supports" title="GitLab">
					GitLab
				</a>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?=APP_CONTEXT?>./js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?=APP_CONTEXT?>./js/materialize.min.js"></script>
	<script type="text/javascript">
	function setLoadingStatus(status) {
		// console.log('Loading Status: ' + status + '%');
		$('.preloader-wrapper.big.active + .loading-info > span').html(status);
		$('.row.center > .row > .progress').css('width', status+'%');
	}
	
	function unScrollTop() {
		if($(document.body).height() - $(document.body).scrollTop() - window.innerHeight > 0) {
			document.body.scrollTop -= 100;
		}
	}
	
	$(document).ready(function() {
		DEFAULT_USER_ICON='<?=DEFAULT_USER_ICON?>';
		if (!<?=$destroyed?'true':'false'?>) {
			console.log('Timer enabled');
			setTimeout(function(){
				window.location.href='./logout.php?timeout=1';
			}, (<?=(SESSION_TIMEOUT)*1000?>)-250);
		}
		Materialize.updateTextFields();
		$('.modal').modal();
		
		setLoadingStatus('100');
		setTimeout(function() {
			$('.modal.user-modal').modal({
				dismissible: true,
				opacity: .5,
				inDuration: 300,
				outDuration: 200,
				startingTop: '4%',
				endingTop: '10%',
				ready: function(modal, trigger) {
					modal.find('.modal-content').load('<?=APP_CONTEXT?>./user_infos.php?user_display_name='+encodeURIComponent(trigger.attr('data-target'))+'&display=modal');
				},
				complete: function(modal, trigger) {
					modal.find('.modal-content').load('loader.php');
				}
			});
			$('.modal.user-modal').find('.modal-content').load('loader.php');
			$('.scroll-date').scrollSpy();
			$('input[type="text"].validate, textarea.materialize-textarea').characterCounter();
			$('input.datepicker').pickadate({
				selectMonths: true,
				selectYears: 4,
				today: 'Today',
				clear: 'Clear',
				close: 'Ok',
				closeOnSelect: false,
				format: 'yyyy-mm-dd'
			});
			$('input.datepicker').attr('readonly', false);
			$('input.datepicker').filter(function() {
				return !!(this.value);
			}).each(function(i, e)  {
				$(e).find('+label').addClass('active');
			});

			$('.preloader-wrapper.big.active').removeClass('active').parent().css('display','none');
			// $('.row.center > .row > .progress > .indeterminate').parent().parent().css('display','none');
			$('.row.content').css('display', 'block');
			if (window.location.hash) {
				unScrollTop();
			}
			<?php if (isset($_GET['support_id'])): ?>
				var elm = document.getElementById('support-<?=$_GET['support_id']?>');
				elm.scrollIntoView(true);
				unScrollTop();
				$(elm).addClass('selected');
				setTimeout(function() {
					$(elm).removeClass('selected');
				}, 10000);
			<?php endif ?>
		}, 500);
	
	});
</script>
