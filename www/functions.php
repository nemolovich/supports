<?php
	// session_start();
	// try {
		// session_start();
	// } catch (Exception $e) {
	// }
	include_once './objects/Support.php';
	include_once './objects/Attachment.php';
	include_once './lib/strptime.php';
	include_once './db_helper.php';
	
	$ini_cfg = parse_ini_file((isset($_ENV["SHARED_CONF"]) ? $_ENV['SHARED_CONF'] : './conf')."/app.ini", true);
	
	$db_cfg = $ini_cfg['data_base'];
	$ldap_cfg = $ini_cfg['ldap'];
	$app_cfg = $ini_cfg['application'];
	$web_cfg = $ini_cfg['web'];
	$srv_cfg = $ini_cfg['server'];
	
	$db_server = $db_cfg['db_server_name'];
	$db_schema = $db_cfg['db_schema'];
	$db_username = $db_cfg['db_user_name'];
	$db_password = $db_cfg['db_password'];
	
	define('DB_SERVER', $db_server);
	define('DB_SCHEMA', $db_schema);
	define('DB_USERNAME', $db_username);
	define('DB_PASSWORD', $db_password);
	
	define("LDAP_MODE", $ldap_cfg['ldap_mode']);
	define("LDAP_SERVER", $ldap_cfg['ldap_server_name']);
	define("LDAP_PORT", $ldap_cfg['ldap_port']);
	define("LDAP_AUTH_NAME", $ldap_cfg['ldap_auth_name']);
	define("BASE_DN", $ldap_cfg['ldap_base_dn']);
	define("DN_USERS", $ldap_cfg['ldap_users_dn'].','.BASE_DN);
	define("USE_LDAP_GROUPS", True);
	define("DN_GROUPS", $ldap_cfg['ldap_groups_dn'].','.BASE_DN);
	define("DN_ADMIN_GROUP", 'cn='.$ldap_cfg['ldap_admin_cn'].','.DN_GROUPS);
	define("DN_TOOL_GROUP", 'cn='.$ldap_cfg['ldap_tool_cn'].','.DN_GROUPS);
	define("DN_DEV_GROUP", 'cn='.$ldap_cfg['ldap_dev_cn'].','.DN_GROUPS);
	
	define('TIME_ZONE', $web_cfg['time_zone']);
	define('SESSION_TIMEOUT', $web_cfg['session_timeout']);
	
	define('LOGIN_PAGE', $app_cfg['login_page']);
	define('LIVE_TIME', $app_cfg['live_time']);
	define('APP_CONTEXT', $app_cfg['app_context']);
	define('APP_TITLE', $app_cfg['app_title']);
	define("ENCODING", $app_cfg['app_encoding']);
	define("DEFAULT_ENCODING", 'UTF-8');
	define('DEFAULT_USER_ICON', $app_cfg['default_user_icon']);
	define('ENCODE_SYSTEM_FILE', in_array(strtolower($app_cfg['encode_system_file']), ["on", "1", "true"]));
	define("MAX_DATES", $app_cfg['max_dates']);
	define('PPT_TYPE', $app_cfg['att_ppt_type']);
	define('RECORD_TYPE', $app_cfg['att_record_type']);
	define('SOURCES_TYPE', $app_cfg['att_src_type']);
	define('PPT_ICON', $app_cfg['att_ppt_icon']);
	define('RECORD_ICON', $app_cfg['att_record_icon']);
	define('SOURCES_ICON', $app_cfg['att_src_icon']);
	define("GROUPS_MODE", $app_cfg['groups_mode']);
	define("GROUPS_MODE_LOCAL", 'local');
	define("GROUPS_MODE_LDAP", 'ldap');
	define("GROUPS_MODE_BOTH", 'both');
	define('ADMIN_USERS', array_filter(explode(',', strtolower($app_cfg['admin_users']))));
	define('TOOL_USERS', array_filter(explode(',', strtolower($app_cfg['tool_users']))));
	define('DEV_USERS', array_filter(explode(',', strtolower($app_cfg['dev_users']))));

	require './utils/messages/utils.php';
	require './utils/ldap/utils.php';
	use utils\Messages;
	use utils\LDAP;

	define('UPLOAD_DIR', $srv_cfg['upload_directory']);
	define('LOCAL_IMG_DIRECTORY', $srv_cfg['local_img_directory']);
	ini_set('upload_max_filesize', $srv_cfg['upload_max_filesize']);
	ini_set('post_max_size', $srv_cfg['post_max_size']);
	ini_set('max_input_time', $srv_cfg['max_input_time']);
	ini_set('max_execution_time', $srv_cfg['max_execution_time']);

	utils\LDAP\User::set_cache_directory(LOCAL_IMG_DIRECTORY);
	utils\LDAP\User::set_app_context(APP_CONTEXT);

	session_set_cookie_params($web_cfg['cookies_time'], APP_CONTEXT);
	date_default_timezone_set(TIME_ZONE);

	$ldap_con = new utils\LDAP\LDAPConnection(LDAP_SERVER, LDAP_PORT, LDAP_MODE);
	
	$destroyed = !isset($_SESSION[utils\LDAP\User::USER]);
	if (isset($_SESSION['SESSION_CREATED']) && time() - $_SESSION['SESSION_CREATED'] - 500 > SESSION_TIMEOUT) {
		if (isset($_SESSION[utils\LDAP\User::USER])) {
			$destroyed = true;
		}
		session_destroy();
		session_unset();
		session_start();
		session_regenerate_id(true);
	}
	$_SESSION['SESSION_CREATED'] = time();
	if (isset($_SESSION[utils\Messages\MSG_ATTR])) {
		$messages = utils\Messages\get_messages();
	} else {
		$messages = array();
	}
	
	/* Errors */
	define("E301", "Error E301 - Support name already exuists");
	define("E302", "Error E302 - SQL query error");

	// var_dump($_SESSION);

	/* 
	 * ------------------------------
	 * Functions
	 * ------------------------------
	 */
	function set_session_messages($messages) {
		global $ldap_con;
		utils\Messages\store_messages(array_merge($ldap_con->get_messages(), $messages));
	}

	function check_access($condition, $msg) {
		global $messages;
		if (!$condition) {
			$messages[] = new utils\Messages\Message($msg, utils\Messages\MessageSeverity::ERROR);
			$_SESSION['redirect_url'] = $_SERVER['REQUEST_URI'];
			set_session_messages($messages);
			echo "<meta http-equiv='refresh' content='0; url=".LOGIN_PAGE."' >";
		}
		return $condition;
	}

	function check_dev() {
		return check_access(is_connected() && (is_dev() || is_tool() || is_admin()),
			"You have to be logged to access to this page.");
	}

	function check_tool() {
		return check_access(is_connected() && (is_dev() || is_tool()),
			"You have to be server tool to access to this page.");
	}

	function check_admin() {
		return check_access(is_connected() && is_admin(),
			"You have to be administrator to access to this page.");
	}

	/**
	 * Check if the current user is connected.
	 *
	 * @return boolean : <code>true</code> if the user is connected,
	 * <code>false</code> otherwise.
	 */
	function is_connected() {
		global $ldap_con;
		return $ldap_con->is_connected();
	}

	/**
	 * @see #is_in_group(string, string)
	 */
	function is_admin($user_id = null) {
		if ($user_id === null) {
			$user_id = get_user_id();
		}
		$local = (GROUPS_MODE === GROUPS_MODE_LOCAL || GROUPS_MODE === GROUPS_MODE_BOTH);
		$ldap = (GROUPS_MODE === GROUPS_MODE_LDAP || GROUPS_MODE === GROUPS_MODE_BOTH);
		$local_ok = ($local && in_array($user_id, ADMIN_USERS)) || !$local;
		$ldap_ok = ($ldap && is_in_group($user_id, DN_ADMIN_GROUP)) || !$ldap;
		return $local_ok && $ldap_ok;
	}

	/**
	 * @see #is_in_group(string, string)
	 */
	function is_tool($user_id = null) {
		if ($user_id === null) {
			$user_id = get_user_id();
		}
		$local = (GROUPS_MODE === GROUPS_MODE_LOCAL || GROUPS_MODE === GROUPS_MODE_BOTH);
		$ldap = (GROUPS_MODE === GROUPS_MODE_LDAP || GROUPS_MODE === GROUPS_MODE_BOTH);
		$local_ok = ($local && in_array($user_id, TOOL_USERS)) || !$local;
		$ldap_ok = ($ldap && is_in_group($user_id, DN_TOOL_GROUP)) || !$ldap;
		return $local_ok && $ldap_ok;
	}

	/**
	 * @see #is_in_group(string, string)
	 */
	function is_dev($user_id = null) {
		if ($user_id === null) {
			$user_id = get_user_id();
		}
		$local = (GROUPS_MODE === GROUPS_MODE_LOCAL || GROUPS_MODE === GROUPS_MODE_BOTH);
		$ldap = (GROUPS_MODE === GROUPS_MODE_LDAP || GROUPS_MODE === GROUPS_MODE_BOTH);
		$local_ok = ($local && (empty(DEV_USERS) || in_array($user_id, DEV_USERS))) || !$local;
		$ldap_ok = ($ldap && is_in_group($user_id, DN_DEV_GROUP)) || !$ldap;
		return $local_ok && $ldap_ok;
	}

	/**
	 * Return the current user DN stored in session.
	 * 
	 * @return {string} : The current user DN.
	 */
	function get_user_dn() {
		return isset($_SESSION[utils\LDAP\User::DN]) ? $_SESSION[utils\LDAP\User::DN] : null;
	}

	/**
	 * Return the current user UID stored in session.
	 * 
	 * @return {string} : The current user UID.
	 */
	function get_user_id() {
		return isset($_SESSION[utils\LDAP\User::UID]) ? strtolower($_SESSION[utils\LDAP\User::UID]) : null;
	}

	function get_logged_user() {
		global $ldap_con;
		$user = null;
		if (is_connected()) {
			$user = utils\LDAP\User::build($_SESSION, $ldap_con->get_ldap_mode());
		}
		return $user;
	}

	/**
	 * Check if an user DN is present as member in a group.
	 * 
	 * @param $user_id {string} : The user UID entry to check
	 * @param $group_dn {string} : The group in which search
	 * @return boolean : <code>true</code> if the user is a member
	 * of the group, <code>false</code> otherwise.
	 */
	function is_in_group($user_id, $group_dn) {
		global $ldap_con;
		$ldap_class = $ldap_con->get_ldap_class();
		$user_dn = $ldap_class::UID.'='.$user_id.','.DN_USERS;
		return $ldap_con->is_in_group($user_dn, $group_dn);
	}

	/**
	 *
	 * @return boolean : <code>true</code> if the connection succeed,
	 * <code>false</code> otherwise.
	 */
	function connect($user, $password, $skip_passwd = false) {
		global $ldap_con;
		return $ldap_con->connect_user(DN_USERS, $user, $password, $skip_passwd);
	}

	/**
	 * Update an user entry.
	 *
	 * @param $user_id {string} : The uid field
	 * @param $first_name {string} : The cn field
	 * @param $last_name {string} : The sn field
	 * @param $mail {string} : The mail field
	 * @param $phone_number {string} : The telephone_number field
	 * @param $mobile_number {string} : The mobile field
	 * @param $job_title {string} : The title field
	 * @param $img_content {string} : The jpegPhoto field
	 * @param $new_password {string} : The userPassword field
	 * @param $new_password_cnf {string} : The password verification
	 * @return boolean : <code>true</code> if the user attributes have
	 * been updated, <code>false</code> otherwise.
	 */
	function update_user($user_id, $first_name, $last_name, $mail, $phone_number, $mobile_number,
		$job_title, $img_content, $new_password, $new_password_cnf, $set_session = false) {
		global $ldap_con;
		return $ldap_con->update_user(DN_USERS, $user_id, $first_name, $last_name, $mail,
			$phone_number, $mobile_number, $job_title, $img_content, $new_password,
			$new_password_cnf, $set_session);
	}

	function add_user($user_id, $group, $first_name, $last_name, $mail) {
		global $ldap_con;
		return $ldap_con->add_user(DN_USERS, DN_GROUPS, $user_id, $group,
			$first_name, $last_name, $mail);
	}

	function remove_user_from_group($user_uid, $group_cn) {
		global $ldap_con;
		return $ldap_con->remove_user_from_group(DN_USERS, DN_GROUPS, $user_uid, $group_cn);
	}

	function remove_user($user_id, $force = false) {
		global $ldap_con;
		return $ldap_con->remove_user(DN_USERS, DN_GROUPS, $user_id, $force);
	}

	function get_users() {
		global $ldap_con;
		return $ldap_con->get_users(DN_USERS);
	}

	function get_user($user_id) {
		global $ldap_con;
		return $ldap_con->get_user(DN_USERS, $user_id);
	}

	function get_user_by_display_name($user_display_name) {
		global $ldap_con;
		return $ldap_con->get_user_by_display_name(DN_USERS, $user_display_name);
	}

	function get_users_with_group() {
		global $ldap_con;
		return $ldap_con->get_users_with_group(DN_USERS, DN_GROUPS);
	}

	function get_groups() {
		global $ldap_con;
		return $ldap_con->get_groups(DN_GROUPS);
	}

	function contact_exists($jid, $list) {
		return in_array(strtolower(trim($jid)), explode(" ", $list));
	}

	function get_month_date($date) {
		// $ttime = strptime(substr($date, 0, 7), '%Y-%m');
		// var_dump($date);
		// var_dump($ttime);
		// var_dump(mktime(0, 0, 0, $ttime['mon'], 1, $ttime['year']+1900)*1000);
		// var_dump(strftime('%Y-%m-%d', mktime(0, 0, 0, $ttime['mon'], 1, $ttime['year']+1900)));
		// return strftime('%Y-%m', mktime(0, 0, 0, $ttime['mon'], 1, $ttime['year']+1900));
		return substr($date, 0, 7);
	}

	function get_int_date($date, $format = '%Y-%m-%d') {
		$ttime = strptimeformat($date, $format);
		// var_dump($date);
		// var_dump($ttime);
		// var_dump(mktime(0, 0, 0, $ttime['mon'], $ttime['mday'], $ttime['year']+1900)*1000);
		// var_dump(strftime('%Y-%m-%d', mktime(0, 0, 0, $ttime['mon'], $ttime['mday'], $ttime['year']+1900)));
		return mktime(0, 0, 0, $ttime['mon'], $ttime['mday'], $ttime['year']+1900);
	}

	function upload_file($file_path, $file_name, $dst_file) {
		global $messages;
		$uploaded = False;
		if (ENCODING != DEFAULT_ENCODING) {
			$enc_file_name = iconv(ENCODING, DEFAULT_ENCODING, $file_name);
		} else {
			$enc_file_name = $file_name;
		}
		if (file_exists($file_path)) {
			$uploaded = move_uploaded_file($file_path, $dst_file);
			if (!$uploaded) {
				$messages[] = new utils\Messages\Message(sprintf("Cannot upload file '%s'", $enc_file_name),
					utils\Messages\MessageSeverity::ERROR);
			}
		} else {
			$messages[] = new utils\Messages\Message(sprintf("Cannot locate file '%s' as '%s'", $enc_file_name, $file_path),
				utils\Messages\MessageSeverity::WARNING);
		}
		return $uploaded;
	}

	function treat_file($file_infos, $a_type) {
		$attachment = null;
		$file_infos_name = $file_infos['name'];
		if (ENCODING != DEFAULT_ENCODING) {
			$file_name = iconv(DEFAULT_ENCODING, ENCODING, $file_infos_name);
		} else {
			$file_name = $file_infos_name;
		}
		$file_dir_enc = UPLOAD_DIR.'/'.$file_name;
		if (ENCODE_SYSTEM_FILE) {
			$file_dir = $file_dir_enc;
		} else {
			$file_dir = UPLOAD_DIR.'/'.$file_infos_name;
		}
		$file_type = $file_infos['type'];
		$file_path = $file_infos['tmp_name'];
		$file_error = $file_infos['error'];
		$file_size = $file_infos['size'];
		$upload_file = './'.$file_dir;
		if (upload_file($file_path, $file_name, $upload_file)) {
			$attachment = new Attachment($a_type, $file_dir_enc);
		}
		return $attachment;
	}

	function remove_file($file_path) {
		global $messages;
		$removed = False;
		if (ENCODING != DEFAULT_ENCODING && ENCODE_SYSTEM_FILE) {
			$enc_file_name = iconv(DEFAULT_ENCODING, ENCODING, $file_path);
		} else {
			$enc_file_name = $file_path;
		}
		$enc_file_path = './'.$enc_file_name;
		if (is_file($enc_file_path)) {
			$removed = unlink($enc_file_path);
		} else {
			$messages[] = new utils\Messages\Message(sprintf("Cannot locate file '%s'", $file_path),
				utils\Messages\MessageSeverity::WARNING);
		}
		return $removed;
		return True;
	}

	function remove_support($support) {
		global $messages;
		$removed = True;
		foreach ($support->attachments as $att) {
			$removed &= remove_file($att->path);
			if (!$removed) {
				$messages[] = new utils\Messages\Message(sprintf("Cannot remove support '%s'", $support->name),
					utils\Messages\MessageSeverity::ERROR);
				break;
			}
		}
		return $removed;
	}

	function is_author($supp, $user_id = null) {
		if ($user_id === null) {
			$user_id = get_user_id();
		}
		return strtolower($supp->sender) === $user_id;
	}

	function get_authors($support) {
		return array_filter(explode(';', preg_replace('/( (et|and|und)|,) /', ';', $support->auth)));
	}
?>