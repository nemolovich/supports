
		<?php include_once './functions.php' ?>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Data sharing">
		<meta name="author" content="Brian GOHIER <brian.gohier@capgemini.com>">
		<meta name="keywords" content="data,sharing">
		<link rel="icon" href="<?=APP_CONTEXT?>./img/favicon.png" sizes="32x32">
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="<?=APP_CONTEXT?>./css/materialize.min.css"  media="screen,projection">
		<link type="text/css" rel="stylesheet" href="<?=APP_CONTEXT?>./css/style.css"  media="screen,projection">
		<link type="text/css" rel="stylesheet" href="<?=APP_CONTEXT?>./css/responsive-list-grid.css"  media="screen,projection">