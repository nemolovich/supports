<?php
	// session_start();
	include_once './functions.php';
	include_once './browsefiles.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include_once "./header.php"; ?>
		
		<title><?=APP_TITLE?></title>
	</head>
	<body>
		<?php include_once "./navbar.php"; ?>
		<?php include_once "./footer.php"; ?>
		<main>
			<div id="modal-users" class="modal user-modal">
				<div class="modal-content">
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat ">Close</a>
				</div>
			</div>
			<?php
				include_once "./checkmessages.php";
				$current_url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
			?>
			<div class="container">
				<?php include_once './loader.php'; ?>
				<div class="row content" style="display:none">
					<div class="side-bar fixed col hide-on-small-only m2 l1">
						<div class="toc-wrapper pinned col s2 menu">
							<span>Presentation&nbsp;dates:</span>
							<ul class="section table-of-contents">
								<?php foreach($dates as $date): ?>
									<?php 
										$int_date = get_int_date($date, '%Y-%m');
									?>
									<li>
										<a href="#<?=$int_date?>" class="<?=array_search($date, $dates)==0?'':''?>">
											<?=strftime("%Y-%m", $int_date)?>
										</a>
									</li>
								<?php endforeach ?>
							</ul>
						</div>
					</div>
					<div class="row elements-container col offset-m2 offset-l1 s12 m8 l10">
						<?php
							$elm = 0;
							$total = sizeof($supports);
						?>
						<script>
							var loading_status = 0;
							var loading_total = <?=$total?>;
						</script>
						<?php foreach($dates as $date): ?>
							<?php 
								$int_date = get_int_date($date, '%Y-%m');
							?>
							<div id="<?=$int_date?>"
								 class="separator col s12 m12 l12 card grid-display scroll-date">
								<blockquote><b><?=strftime("%Y-%m", $int_date)?></b></blockquote>
								<div class="row">
									<?php foreach($content[$date] as $arr): ?>
										<script>
											var loading_status = Math.round(<?=(++$elm)?>/loading_total*100);
											setLoadingStatus(loading_status);
										</script>
										<div id="support-<?=$arr->id?>" class="element s12 m6 l4 card blue-grey darken-1 hoverable">
											<!--div class="card-content white-text s12 m3 l6 gs12"-->
											<div class="card-content white-text gs12">
												<div class="content-left">
													<span title="<?=$arr->name?>" class="card-title">
														<?=$arr->name?>
													</span>
													<a href="javascript:copyToClipboard('<?=$current_url?>?support_id=<?=$arr->id?>');"
													   title="Copy support link to clipboard" class="copy-link">
														<i class="material-icons">content_copy</i>
													</a>
													<p>
														<?php
															$authors=$arr->auth;
															$auth_n = 0;
														?>
														<?php foreach(get_authors($arr) as $auth): ?>
															<?php
																$modal_id = $arr->id.'-'.(++$auth_n);
																$authors = preg_replace('/('.$auth.')/', 
																'<a class="waves-effect waves-light modal-trigger"'.
																	'data-target="'.$auth.'" href="#modal-users">\1</a>', $authors);
															?>
														<?php endforeach ?>
														<?=$authors?>
													</p>
													<span class="grid-hide">Presented on </span><span><?=$arr->date?></span>
												</div>
											</div>
											<?php
												$s_atts = $arr->attachments;
												uasort($s_atts, function($a, $b) {
													return strcmp($a->type, $b->type);
												});
											?>
											<?php foreach($s_atts as $att): ?>
												<div class="card-action white-text sleft">
													<a href="<?=APP_CONTEXT.($att->path)?>" title="Download <?=($att->type)?>"
													   class="att-link" target="_blank">
														<i class="material-icons"><?=
															$att->type === PPT_TYPE ? PPT_ICON :
															($att->type === RECORD_TYPE ? RECORD_ICON :
															($att->type === SOURCES_TYPE ? SOURCES_ICON :
															'file_download'))
														?></i><?=($att->type)?>
													</a>
												</div>
											<?php endforeach ?>

											<?php if (is_connected() && (is_dev() || is_tool()
														|| is_admin())): ?>
												<div class="action-buttons <?=(is_admin())?'admin':''?>">
													<form action="<?=APP_CONTEXT?>./edit.php" method="post">
														<input name="edit_support_id" type="hidden"
															   value="<?=$arr->id?>">
														<button type="submit" class="btn btn-small transparent hover-green"
																value="Edit" title="Edit <?=$arr->name?> support">
															<i class="material-icons">mode_edit</i>
														</button>
													</form>
													<?php //if (is_admin() || is_author($arr)): ?>
													<?php if (is_admin()): ?>
														<form action="<?=APP_CONTEXT?>./db_action.php" method="post">
															<input name="php_action" type="hidden" value="delete_support">
															<input name="support_id" type="hidden" value="<?=$arr->id?>">
															<button type="submit" class="btn btn-small transparent hover-red"
																	value="Delete" title="Delete <?=$arr->name?> support"
																	onclick="return confirm('Do you really want to permanently delete this support ?');">
																<i class="material-icons">delete</i>
															</button>
														</form>
													<?php endif ?>
												</div>
											<?php endif ?>
										</div>
									<?php endforeach ?>
								</div>
							</div>
						<?php endforeach ?>
					</div>
					<div class="side-bar col hide-on-small-only m2 l1">
						<div class="toc-wrapper pinned col s2">

							<form action="#">
								<div>
									<span>Search</span>
									<input type="search" id="filter-field" results=5
										   onsearch="onkeyup(this);" onkeyup="filter_search(this);"
										   elment-selector=".element"
										   filter-selector=".card-content"
										   parent-selector=".row>.separator.card"
										   underline-class="underline"
										   hide-on-filter="true"
										   hide-class="element-hidden"
										   counter-id="filter-counter"
										   counter-message="Found <b>${counter}</b> matching supports"
										   one-result-by-block="true"
										   placeholder="Search for Title/Author/Date">
								</div>
								<div>
									<span>Filter by content</span>
									<div>
										<!--p>
										  <input type="checkbox" id="filtered-ppt" checked
												 readonly disabled class="enabled">
										  <label for="filtered-ppt"><=PPT_TYPE?></label>
										</p-->
										<p>
											<input type="checkbox" id="filtered-record"
												   onchange="filter_type(this, '<?=RECORD_TYPE?>');">
											<label for="filtered-record"><?=RECORD_TYPE?></label>
										</p>
										<p>
											<input type="checkbox" id="filtered-src"
												   onchange="filter_type(this, '<?=SOURCES_TYPE?>');">
											<label for="filtered-src"><?=SOURCES_TYPE?></label>
										</p>
									</div>
								</div>
								<div>
									<span id="filter-counter" class="character-counter"
										  hide-on-empty="false"></span>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
			<div class="col s12 clear-bottom">
			</div>
		</main>

		<input type="text" id="copy-input"
			   style="display:block;width:1px;height:1px;opacity:0;position:absolute;margin-left:-1000px;"></input>
		<script type="text/javascript">
			function copyToClipboard(content) {
				var temp = $("#copy-input");
				$("body").append(temp);
				temp.val(content);
				temp.select();
				console.log(document.location.href);
				document.execCommand("copy");
			}
		</script>
		<script type="text/javascript" src="<?=APP_CONTEXT?>./js/grid.js"></script>
		<script type="text/javascript" src="<?=APP_CONTEXT?>./js/filter.js"></script>
		<script type="text/javascript" src="<?=APP_CONTEXT?>./js/search-filter.js"></script>
	</body>
</html>
