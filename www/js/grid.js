
var GRID_DISPLAY = 'grid';
var LIST_DISPLAY = 'list';
var DISPLAY_AS = GRID_DISPLAY;
var DISPLAY_AS_COOKIE = 'displayas';
var DEFAULT_ANIMATION_TIME = 100;
var ANIMATION_TIME = DEFAULT_ANIMATION_TIME;

function setFilter() {
	
	var butt = $('.filter.btn.btn-small.btn-display');
	butt.find('.material-icons').html(DISPLAY_AS === LIST_DISPLAY ? 'view_module' : 'view_list');
	document.cookie = DISPLAY_AS_COOKIE + '=' + DISPLAY_AS;
}

$(document).ready(function() {
	var cookies = document.cookie.split(/;\s*/);
	for(c in cookies) {
		var l = cookies[c].split('=');
		if (l[0] === DISPLAY_AS_COOKIE) {
			DISPLAY_AS = l[1];
			break;
		}
	}
	$('.elements-container>div.separator>div>div').addClass(DISPLAY_AS+'-display');
	setFilter();
});

var TIME_OUT;
function changeClass(elmList, index, toAdd, toRemove, delay, timeDiff) {
	if (!delay) {
		delay = DEFAULT_ANIMATION_TIME;
	}
	var elmLen = elmList.length;
	var currIndex = index;
	
	if (TIME_OUT) {
		clearTimeout(TIME_OUT);
	}
	if (index === elmList.length) {
		return;
	}
	var elm = $(elmList[index]);
	var displayedDelay = elm.hasClass('element-hidden') || elm.css('display') === "none" ? 0 : delay;
	elm.removeClass(toRemove);
	elm.addClass(toAdd);
	TIME_OUT = setTimeout(function() {
		changeClass(elmList, ++index, toAdd, toRemove,
			(delay - delay/timeDiff), timeDiff*1.5);
		}, displayedDelay);
}

function changeFilter() {
	DISPLAY_AS = DISPLAY_AS === GRID_DISPLAY ? LIST_DISPLAY : GRID_DISPLAY;
	var to_remove, to_add;
	
	var elms = $('.elements-container>div.separator>div>div');
	var acceleration = 4;
	if (DISPLAY_AS === LIST_DISPLAY) {
		to_remove = GRID_DISPLAY + '-display';
		to_add = LIST_DISPLAY + '-display';
		acceleration = 6;
	} else {
		to_remove = LIST_DISPLAY + '-display';
		to_add = GRID_DISPLAY + '-display';
	}
	
	changeClass(elms, 0, to_add, to_remove, ANIMATION_TIME, acceleration);
	
	setFilter();
}
