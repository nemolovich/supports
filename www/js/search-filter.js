var INPUT_SEARCH_FILTER = document.getElementById('filter-field');

function filter_search(elm) {
	applyFilter(elm);
}
function filter_type(elm, filterTxt) {
	var hiddenClass = 'element-hidden';
	var hiddenClassType = hiddenClass + '-' + filterTxt;
	var selected = elm.checked;

	if (selected) {
		$('.row>.separator.card .element').removeClass('element').addClass(hiddenClass).addClass(hiddenClassType);
		$('.row>.separator.card .' + hiddenClass).addClass(hiddenClassType);
		$('.row>.separator.card .' + hiddenClassType).each(function(idx, e) {
			var actions = $(e).find('.card-action a').clone().children().remove().end().text();
			var className = e.className
			if (actions.indexOf(filterTxt) !== -1) {
				$(e).removeClass(hiddenClassType).removeClass(hiddenClassType).filter(function() {
					return !this.className.match(/element-hidden-\w+/);
				}).removeClass(hiddenClass).addClass('element');
			}
		});
	} else {
		$('.row>.separator.card .' + hiddenClassType).removeClass(hiddenClassType).filter(function() {
			return !this.className.match(/element-hidden-\w+/);
		}).removeClass(hiddenClass).addClass('element');
	}
	filter_search(INPUT_SEARCH_FILTER);
}

$(document).ready(function() {
	filter_search(INPUT_SEARCH_FILTER);
});

