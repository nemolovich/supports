<?php

	function get_strptime_month($m) {
		return intval($m ? $m : 1);
	}

	function get_strptime_year($y) {
		return  $y > 1900 ? $y - 1900 : 0;
	}

	function strptimeformat($date, $format) {
		$letters = array(
			'S' => array(
				'type' => 'sec',
				'func' => 'intval',
				'size' => 2,
				'deft' => 0
			),
			'M' => array(
				'type' => 'min',
				'func' => 'intval',
				'size' => 2,
				'deft' => 0
			),
			'H' => array(
				'type' => 'hour',
				'func' => 'intval',
				'size' => 2,
				'deft' => 0
			),
			'd' => array(
				'type' => 'mday',
				'func' => 'intval',
				'size' => 2,
				'deft' => 1
			),
			'm' => array(
				'type' => 'mon',
				'func' => 'get_strptime_month',
				'size' => 2,
				'deft' => 1
			),
			'Y' => array(
				'type' => 'year',
				'func' => 'get_strptime_year',
				'size' => 4,
				'deft' => 0
			)
		);
		
		$pattern = '/%(?P<L>\w)/';
		preg_match_all($pattern, preg_quote($format), $m);
		$masks = array();
		foreach ($m['L'] as $l) {
			$masks['%'.$l] = sprintf('(?P<%s>[0-9]{%d})', $l, $letters[$l]['size']);
		}

		$rexep = "#".strtr(preg_quote($format), $masks)."#";
		if(!preg_match($rexep, $date, $out)) {
			return false;
		}

		$ret = array();
		foreach (array_keys($letters) as $l) {
			if (array_key_exists($l, $out)) {
				$ret[$letters[$l]['type']] = $letters[$l]['func']($out[$l]);
			} else {
				$ret[$letters[$l]['type']] = $letters[$l]['func']($letters[$l]['deft']);
			}
		};
		return $ret;
	}
?>
