
			<div class="row center">
				<div class="row blue lighten-4">
					<div class="progress blue">
						<div class="determinate"></div>
					</div>
				</div>
				<div class="preloader-wrapper big active">
					<div class="spinner-layer spinner-blue-only">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div><div class="gap-patch">
							<div class="circle"></div>
						</div><div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<div class="loading-info">
					Loading: <span>0</span>%
				</div>
			</div>