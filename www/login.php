<?php
	// session_start();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<?php include_once "./header.php"; ?>
		<title><?=APP_TITLE?> - Authentication</title>
	</head>
	<body>
		<?php include_once "./navbar.php"; ?>
		<?php include_once "./footer.php"; ?>
		<main>
			<?php include_once "./checkmessages.php"; ?>
			<div class="container">
				<?php include_once './loader.php'; ?>
				<div class="row content">
					<form action="<?php print $_SERVER['PHP_SELF']; ?>" method="post" name="loginForm">
						<h3 class="form-signin-heading">Authentication</h3>
						<div>
							You can connect with <?=LDAP_AUTH_NAME?>.
						</div>

						<?php
							if (isset($_SESSION['redirect_url'])) {
								$redirect_url = $_SESSION['redirect_url'];
							} else {
								$redirect_url = APP_CONTEXT;
							}
							if (isset($_POST['username'])) {
								// $connected = connect($_POST['username'], $_POST['password'], true);
								$connected = connect($_POST['username'], $_POST['password']);
								if (!$connected) {
									$messages[] = new utils\Messages\Message("Connection error", utils\Messages\MessageSeverity::ERROR);
									$redirect_url = LOGIN_PAGE;
								}
								set_session_messages($messages);
								echo "<meta http-equiv='refresh' content='0; url=$redirect_url' >";
							}
						?>
						
						
						<input name="username" type="text" class="form-control" placeholder="LDAP User Login or Email" autofocus="" required="">
						<input name="password" type="password" class="form-control" placeholder="Password" required="">
						
						<button class="btn btn-large blue" name="Submit" value="Login" type="submit">Login</button>
					</form>
				</div>
			</div>
			<div class="col s12 clear-bottom">
			</div>
		</main>
	</body>
</html>
