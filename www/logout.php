<?php
	// session_start();
	error_reporting(0);
	include_once "./functions.php";
	if (is_connected()) {
		session_destroy();
		if (isset($_GET['timeout'])) {
			$message = new utils\Messages\Message('Session timeout', utils\Messages\MessageSeverity::WARNING);
		} else {
			$message = new utils\Messages\Message('Successfully disconnected');
		}
	} else {
		$message = new utils\Messages\Message('Disconnect error.', utils\Messages\MessageSeverity::ERROR);
	}
	session_unset();
	session_regenerate_id(true);
	session_start();
	$messages = [];
	$messages[] = $message;
	set_session_messages($messages);
	header("Refresh:0; url=".LOGIN_PAGE);
	error_reporting(E_ALL);
?>
