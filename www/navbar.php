
	<header class="small">
		<div class="navbar-fixed">
			<nav class="light-blue lighten-1">
				<div class="nav-wrapper">
					<a href="<?=APP_CONTEXT?>" class="brand-logo left">
						<img class="circle responsive-img hide-on-med-and-down" src="<?=APP_CONTEXT?>img/logo_ntic.png">
						<span><?=APP_TITLE?></span>
					</a>
					<ul id="nav-mobile" class="right">
						<?php if (isset($is_index)): ?>
							<li>
								<a href="#"
								   class="btn btn-small filter btn-display waves-effect waves-light blue-grey darken-1  hide-on-small-only"
								   onclick="changeFilter();return false;"
								   title="Switch display grid/list">
									<i class="material-icons">view_list</i>
								</a>
							</li>
						<?php endif ?>
						<?php if (is_connected()): ?>
							<?php if (is_dev() ||  is_tool() || is_admin()): ?>
								<li>
									<a href="<?=APP_CONTEXT.'upload.php'?>"
									   class="btn btn-small waves-effect waves-light"
									   title="Upload a support">
										<i class="material-icons right">file_upload</i>
										<span class="hide-on-small-only">Upload</span>
									</a>
								</li>
							<?php endif ?>
							<li class="user">
								<?php 
									$user = get_logged_user();
									if ($user->image != null) {
										$img_data = $user->image;
									} else {
										$img_data = DEFAULT_USER_ICON;
									}
								?>
								<a href="<?=APP_CONTEXT.'logout.php'?>"
								   class="btn btn-small waves-effect waves-light red lighten-2"
								   title="Close your session">
									<span class="hide-on-small-only">Logout</span>
									<img class="user-icon responsive-img circle" src="<?=$img_data?>">
								</a>
								<span class="user-infos z-depth-4">
									You are logged as:<br/>
									<b><?=$user->display_name?></b>
								</span>
							</li>
						<?php else: ?>
							<li>
								<a href="<?=LOGIN_PAGE?>"
								   class="btn btn-small waves-effect waves-light green lighten-2"
								   title="Connect to your session">
									Login
								</a>
							</li>
						<?php endif ?>
					</ul>
				</div>
			  </nav>
		</div>
	</header>