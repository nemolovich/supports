<?php
class Attachment
{
	public $id;
	public $type;
	public $path;
	
	function __construct($type, $path = null) {
		$this->type = $type;
		if ($path !== null) {
			$this->path = $path;
		}
	}
}
?>
