<?php
class Support
{
	public $id;
	public $name;
	public $date;
	public $sender;
	public $auth = '<i>NTIC Community</i>';
	public $attachments = [];

	function __construct($name) {
		$this->name = $name;
	}

	function add_attachment($att) {
		$this->attachments[] = $att;
	}

	function remove_attachment($att) {
		$this->attachments = array_udiff($this->attachments, array($att), function($a, $b) {
								return strcmp($a->type, $b->type);
							});
	}

	function has_attachment($att_type) {
		$exists = False;
		foreach ($this->attachments as $att) {
			if ($att->type === $att_type) {
				$exists = True;
				break;
			}
		}
		return $exists;
	}

	function update_attachment($att_dst) {
		if ($att_dst !== null) {
			foreach ($this->attachments as $att) {
				if ($att->type === $att_dst->type) {
					$att->type = $att_dst->type;
					// Check if it's new one
					if ($att_dst->id === null) {
						$att->path = $att_dst->path;
					} else {
						if (ENCODING != DEFAULT_ENCODING) {
							$att->path = iconv(DEFAULT_ENCODING, ENCODING, $att_dst->path);
						} else {
							$att->path = $att_dst->path;
						}
					}
					break;
				}
			}
		}
	}
}
?>
