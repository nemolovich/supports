<?php
	// session_start();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<?php include_once "./header.php"; ?>
		<title><?=APP_TITLE?> - Upload a support</title>
	</head>
	<body>

		<?php include_once "./navbar.php"; ?>
		<?php include_once "./footer.php"; ?>
		<main>
			<?php include_once "./checkmessages.php"; ?>
			<?php 
				if (!check_dev()) {
					die('Access denied');
				}
			?>
			<div class="container">
				<?php include_once './loader.php'; ?>
				<div class="row content">
					<form enctype="multipart/form-data" action="<?php print $_SERVER['PHP_SELF']; ?>"
						  method="post" name="uploadForm">
						<h3 class="form-signin-heading">Upload a support</h3>

						<?php
						
							$user = get_logged_user();
							$support_name = '';
							$enc_support_name = '';
							$support_auth = '';
							$enc_support_auth = '';
							$support_date = '';
							if (isset($_POST['supportName'])) {
								$enc_support_name = $_POST['supportName'];
								$enc_support_auth = $_POST['supportAuth'];
								if (ENCODING != DEFAULT_ENCODING) {
									$support_name = iconv(DEFAULT_ENCODING, ENCODING, $enc_support_name);
									$support_auth = iconv(DEFAULT_ENCODING, ENCODING, $enc_support_auth);
								} else {
									$support_name = $enc_support_name;
									$support_auth = $enc_support_auth;
								}
								$support_sender = $user->uid;
								$support_date = $_POST['supportDate'];
								
								$_SESSION['supportName'] = $enc_support_name;
								$_SESSION['supportAuth'] = $enc_support_auth;
								$_SESSION['supportDate'] = $support_date;
								
								$exists = support_exists($support_name);
								if ($exists) {
									$messages[] = new utils\Messages\Message(E301, utils\Messages\MessageSeverity::ERROR);
									set_session_messages($messages);
									echo "<meta http-equiv='refresh' content='0'>";
									die('Already exists');
								}
								
								$support = new Support($support_name);
								$support->auth = $support_auth;
								$support->sender = $support_sender;
								// $support->date = strftime('%Y-%m-%d', time());
								$support->date = $support_date;
								
								foreach ($_FILES as $f_type => $f_infos) {
									if ($f_type == 'supportppt' && !empty($f_infos['name'])) {
										$a_type = PPT_TYPE;
										$attachment = treat_file($f_infos, $a_type);
										$support->add_attachment($attachment);
									} else if ($f_type == 'supportvideo' && !empty($f_infos['name'])) {
										$a_type = RECORD_TYPE;
										$attachment = treat_file($f_infos, $a_type);
										$support->add_attachment($attachment);
									} else if ($f_type == 'supportsources' && !empty($f_infos['name'][0])) {
										$a_type = SOURCES_TYPE;
										if (sizeof($f_infos['name']) > 1) {
											$zipdir = UPLOAD_DIR.'/'.$support_name.'.zip';
											$zippath = './'.$zipdir;
											if (ENCODING != DEFAULT_ENCODING) {
												$enc_zippath = iconv(ENCODING, DEFAULT_ENCODING, $zipdir);
											} else {
												$enc_zippath = $zipdir;
											}
											$zip = new ZipArchive;
											$zip->open($enc_zippath, ZipArchive::CREATE);
											for ($i=0; $i<sizeof($f_infos['name']); $i++) {
												$file_name = $f_infos['name'][$i];
												$file_name = $file_name;
												$file_type = $f_infos['type'][$i];
												$file_path = $f_infos['tmp_name'][$i];
												$file_error = $f_infos['error'][$i];
												$file_size = $f_infos['size'][$i];
												if (file_exists($file_path)) {
													$zip->addFile($file_path, $file_name);
												} else {
													$messages[] = new utils\Messages\Message(sprintf("Cannot locate file '%s' as '%s'", $file_name, $file_path),
														utils\Messages\MessageSeverity::WARNING);
												}
											}
											$zip->close();
											$attachment = new Attachment($a_type);
											$attachment->path = $zipdir;
											$support->add_attachment($attachment);
										} else {
											if (!empty($f_infos['name'][0])) {
												if (ENCODING != DEFAULT_ENCODING) {
													$file_name = iconv(DEFAULT_ENCODING, ENCODING, $f_infos['name'][0]);
												} else {
													$file_name = $f_infos['name'][0];
												}
												$file_dir = UPLOAD_DIR.'/'.$file_name;
												$file_type = $f_infos['type'][0];
												$file_path = $f_infos['tmp_name'][0];
												$file_error = $f_infos['error'][0];
												$file_size = $f_infos['size'][0];
												$upload_file = './'.$file_dir;
												if (upload_file($file_path, $file_name, $upload_file)) {
													$attachment = new Attachment($a_type);
													$attachment->path = $file_dir;
													$support->add_attachment($attachment);
												}
											}
										}
									}
								}
								
								if (insert_support($support)) {
									$messages[] = new utils\Messages\Message(sprintf("Support '%s' successfully uploaded", $enc_support_name),
										utils\Messages\MessageSeverity::SUCCESS);
									unset($_SESSION['supportName']);
									unset($_SESSION['supportAuth']);
									unset($_SESSION['supportDate']);
								} else {
									$messages[] = new utils\Messages\Message(sprintf("Support '%s' could not be uploaded", $enc_support_name),
										utils\Messages\MessageSeverity::ERROR);
								}
								
								set_session_messages($messages);
								
								echo "<meta http-equiv='refresh' content='0'>";
							}
						?>

						<div class="row">
							<div class="col s12">
								<div class="input-field">
									<input id="support-name" name="supportName" type="text" maxlength="100"
										   class="validate" value="<?=$enc_support_name?>"
										   autofocus="" required="" data-length="100">
									<label for="support-name">Name for this support</label>
								<div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="input-field">
									<input id="support-auth" name="supportAuth" type="text" maxlength="100"
										   class="validate" value="<?=$enc_support_auth?>"
										   required="" data-length="100">
								  <label for="support-auth">Presentation author(s)</label>
								  <span class="validity"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<div class="input-field">
									<input id="support-date" name="supportDate" type="date"
										   class="datepicker validate" required=""
										   pattern="20[0-9]{2}-(0[0-9]|1[0-2])-([0-2][0-9]|3[01])">
									<label for="support-date">Presentation date</label>
								</div>
							</div>
						</div>
						<div class="input-field file-field">
							<div class="btn btn-small large">
								<span>Browse a PowerPoint</span>
								<input type="file" name="supportppt" 
									   required="" accept=".ppt, .pptx">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="Upload presentation" disabled readonly>
							</div>
						</div>
						<div class="input-field file-field">
							<div class="btn btn-small large">
								<span>Browse a record</span>
								<input type="file" name="supportvideo" accept="video/*">
								<!--input type="file" name="supportvideo"-->
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="Upload video" disabled readonly>
							</div>
						</div>
						<div class="input-field file-field">
							<div class="btn btn-small large">
								<span>Browse sources</span>
								<input type="file" name="supportsources[]" multiple>
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="Upload multiple sources files" disabled readonly>
							</div>
						</div>
						<input type="hidden" name="MAX_FILE_SIZE" value="1000000000">
						<button class="btn btn-large blue" name="Submit" value="Upload"
								type="submit">
							Submit
						</button>
						<button class="btn grey" name="Cancel" value="Cancel" type="reset"
							onclick="forms['uploadForm'].reset();location.href='<?=APP_CONTEXT?>./reset.php';">
							Cancel
						</button>
					</form>
				</div>
			</div>
			<div class="col s12 clear-bottom">
			</div>
		</main>
	</body>
</html>
