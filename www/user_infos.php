<?php if (!isset($_GET['display']) || $_GET['display'] !== 'modal'): ?>
<!DOCTYPE html>
<html>
	<head>
<?php endif ?>
		<?php include_once './header.php'; ?>
<?php if (!isset($_GET['display']) || $_GET['display'] !== 'modal'): ?>
	</head>
	<body style="width: 50%;margin: auto;">
<?php endif ?>
<?php 
	if (isset($_GET['user_uid'])) {
		$user_id = $_GET['user_uid'];
	} elseif (isset($set_user_id)) {
		$user_id = $set_user_id;
	} elseif (isset($_GET['user_display_name'])) {
		$user_name = $_GET['user_display_name'];
	} elseif (isset($set_user_display_name)) {
		$user_name = $set_user_display_name;
	}
	$user = null;
?>
<?php if (isset($user_id) || isset($user_name)): ?>
		<div class="user-details row">
			<?php
				$user = isset($user_id) ? get_user($user_id) : get_user_by_display_name($user_name);
				$user_display_name = $user->first_name.' '.$user->last_name;
				if ($user->image != null) {
					$img_data = $user->image;
				} else {
					$img_data = DEFAULT_USER_ICON;
				}
			?>
			<?php if ($user->uid !== null): ?>
				<div class="card horizontal">
					<div class="card-image col s3">
						<img class="user-image responsive-img" src="<?=$img_data?>">
					</div>
					<div class="card-stacked col s9">
						<div class="card-content">
							<span class="card-title grey-text text-darken-4">
								<?=$user_display_name?>
							</span>
							<?php if ($user->title): ?>
								<span class="grey-text">
									<?=$user->title?>
								</span>
							<?php endif ?>
							<a href="mailto:<?=$user->mail?>"
							   title="Send a mail to <?=$user_display_name?>">
							   <i class="material-icons">mail</i>
							   <?=$user->mail?>
							</a>
							<a href="tel:<?=$user->phone_number?>"
							   title="Call <?=$user_display_name?>">
							   <i class="material-icons">dialer_sip</i>
							   <?=$user->phone_number?>
							</a>
							<a href="sip:<?=$user->mail?>"
							   title="Chat with <?=$user_display_name?>">
							   <i class="material-icons">chat</i>
							   Skype
							</a>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
<?php endif ?>
<?php if ($user === null || $user->uid === null): ?>
	<div class="row">
		<h5 class="col s4 center offset-s4 red-text">Cannot find user</h5>
		<img class="responsive-img col s4 center offset-s4" src="<?=APP_CONTEXT?>./img/404.png">
	</div>
<?php endif ?>
<?php if (!isset($_GET['display']) || $_GET['display'] !== 'modal'): ?>
	</body>
</html>
<?php endif ?>