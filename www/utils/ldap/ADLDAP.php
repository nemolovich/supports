<?php
namespace utils\LDAP;

class ADLDAP {

	const UID = 'CN';
	const SEARCH_IDS = [self::UID, self::MAIL];
	const SEARCH_FILTERS = ['objectClass=organizationalPerson',
							'objectClass=person',
							'objectClass=top',
							'objectClass=user'];
	const FIRST_NAME = 'givenName';
	const LAST_NAME = 'sn';
	const DISPLAY_NAME = 'displayName';
	const MAIL = 'mail';
	const PHONE_NUMBER = 'telephoneNumber';
	const MOBILE_NUMBER = 'mobile';
	const TITLE = 'capgemini-jobRole';
	const IMAGE = 'thumbnailPhoto';
	const INITIALS = null;
	const PASSWORD = 'userPassword';

	function __construct() {
		throw new NotImplementedException();
	}

	static function _get_class() {
		return get_class();
	}
}
?>
