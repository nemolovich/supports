<?php
namespace utils\LDAP;
use utils\Messages\Message as Message;
use utils\Messages\MessageSeverity as MessageSeverity;
use function utils\Messages\clone_message;
require 'LDAPModes.php';
require 'OpenLDAP.php';
require 'ADLDAP.php';
require 'User.php';

class LDAPConnection {

	private $ldap_server;
	private $ldap_port;
	private $ldap_mode;
	private $messages = [];

	function __construct($ldap_server, $ldap_port, $ldap_mode = LDAPModes::__default) {
		$this->ldap_server = $ldap_server;
		$this->ldap_port   = $ldap_port;
		$this->ldap_mode   = $ldap_mode;
	}

	function get_ldap_class() {
		if ($this->ldap_mode === LDAPModes::OPEN_LDAP) {
			$OBJ = OpenLDAP::_get_class();
		} elseif ($this->ldap_mode === LDAPModes::AD_LDAP) {
			$OBJ = ADLDAP::_get_class();
		} else {
			throw new NotImplementedException();
		}
		return $OBJ;
	}

	/**
	 * Return the current connection LDAP mode.
	 * 
	 * @return {LDAPModes} : The LDAP mode.
	 */
	function get_ldap_mode() {
		return $this->ldap_mode;
	}

	/**
	 * Return the current LDAP messages.
	 * 
	 * @return {array} : List of connection Messages.
	 */
	function get_messages() {
		return $this->messages;
	}

	/**
	 * Set the current LDAP messages.
	 * 
	 * @param $messages {array} : List of Messages.
	 */
	function set_messages($messages) {
		$this->messages = [];
		if ($messages && sizeof($messages) > 0) {
			foreach ($messages as $m) {
				$this->messages[] = clone_message($m);
			}
		}
	}

	/**
	 * Return appripriate filter for LDAP search with current
	 * LDAPModes mode.
	 * 
	 * @return {string} : The LDAP search filter.
	 */
	private function get_search_filter() {
		$ldap_class = $this->get_ldap_class();

		return '('.implode(')(', $ldap_class::SEARCH_FILTERS).')';
	}

	/**
	 * Return appropriate filter for LDAP search with current
	 * LDAPModes mode.
	 * 
	 * @param $user_id {string} : The LDAP UID/MAIL user to look for.
	 * @return {string} : The LDAP search filter.
	 */
	private function get_search_filter_with_mail($user_id) {
		$search_value = strtolower($user_id);
		$ldap_class = $this->get_ldap_class();

		$filters = '(&'.$this->get_search_filter().')';
		$ids = '(|('.implode("=$search_value)(", $ldap_class::SEARCH_IDS)."=$search_value))";
		$res = "(&$filters$ids)";
		// var_dump($res);
		return $res;
	}

	/**
	 * Return appropriate filter for LDAP search with current
	 * LDAPModes mode.
	 * 
	 * @param $user_id {string} : The LDAP UID user to look for.
	 * @return {string} : The LDAP search filter.
	 */
	private function get_search_filter_id($user_id) {
		$search_value = strtolower($user_id);
		$ldap_class = $this->get_ldap_class();

		$filters = $this->get_search_filter();
		$id = '('.$ldap_class::UID."=$search_value)";
		$res = "(&$filters$id)";
		return $res;
	}

	/**
	 * Check if the current user is connected.
	 *
	 * @return boolean : <code>true</code> if the user is connected,
	 * <code>false</code> otherwise.
	 */
	function is_connected() {
		return isset($_SESSION[User::DN]);
	}

	/**
	 * Check if an user DN is present as member in a group.
	 * 
	 * @param $user_dn {string} : The user DN entry to check
	 * @param $group_dn {string} : The group in which search
	 * @return boolean : <code>true</code> if the user is a member
	 * of the group, <code>false</code> otherwise.
	 */
	function is_in_group($user_dn, $group_dn) {
		error_reporting(0);

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		$pic_group_search = ldap_search($con, $group_dn, "(member=$user_dn)");
		$entries = ldap_get_entries($con, $pic_group_search);

		if ($entries == null || empty($entries) || $entries['count'] === 0) {
			ldap_close($con);
			return false;
		}

		ldap_close($con);
		error_reporting(E_ALL);
		return true;
	}

	/**
	 * Connect a user with credentials and set session for user.
	 *
	 * @param $users_dn {string} : The user DN
	 * @param $user {string} : The user ID or user mail
	 * @param $password {string} : The user password
	 * @return boolean : <code>true</code> if the connection succeed,
	 * <code>false</code> otherwise.
	 */
	function connect_user($users_dn, $user, $password, $skip_pass_verif = false) {
		error_reporting(0);

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		/* Try to connect to server */
		$op = fsockopen($this->ldap_server, $this->ldap_port, $errno, $errstr, 2);
		if (!$op) {
			$this->messages[] = new Message(E001, MessageSeverity::ERROR);
			return false;
		} else {
			fclose($op);
		}

		/* bind anon and find user by uid */
		$user_search = ldap_search($con, $users_dn, $this->get_search_filter_with_mail($user));
		$user_get = ldap_get_entries($con, $user_search);
		$user_entry = ldap_first_entry($con, $user_search);
		$user_dn = ldap_get_dn($con, $user_entry);

		if (!$user_get || empty($user_get) || $user_get['count'] === 0) {
			$this->messages[] = new Message(E101, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		/* Test token binding */
		if (!$skip_pass_verif && ldap_bind($con, $user_dn, $password) === false) {
			$this->messages[] = new Message(E101, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		/* And Finally, Assign user */
		if (!isset($_SESSION['SESSION_CREATED'])) {
			$_SESSION['SESSION_CREATED'] = time();
		} else if (time() - $_SESSION['SESSION_CREATED'] > 1800) {
			session_regenerate_id(true);
			$_SESSION['SESSION_CREATED'] = time();
		}
		
		ldap_close($con);
		$user = new User($user_get[0], $user_dn, $this->ldap_mode);
		$this->messages[] = new Message("Successfully logged as <b>".$user->display_name."</b>", MessageSeverity::SUCCESS);

		User::set_session($user);

		error_reporting(E_ALL);
		return true;
	}

	/**
	 * Update an user entry.
	 * WARNING: Only OpenLDAP is implemented.
	 *
	 * @param $users_dn {string} : The Users DN to apply
	 * @param $user_id {string} : The uid field
	 * @param $first_name {string} : The cn field
	 * @param $last_name {string} : The sn field
	 * @param $mail {string} : The mail field
	 * @param $phone_number {string} : The telephoneNumber field
	 * @param $mobile_number {string} : The mobile field
	 * @param $job_title {string} : The title field
	 * @param $img_content {string} : The jpegPhoto field
	 * @param $new_password {string} : The userPassword field
	 * @param $new_password_cnf {string} : The password verification
	 * @return boolean : <code>true</code> if the user attributes have
	 * been updated, <code>false</code> otherwise.
	 */
	function update_user($users_dn, $user_id, $first_name, $last_name, $mail = null,
			$phone_number = null, $mobile_number = null, $job_title = null,
			$img_content = null, $new_password = null, $new_password_cnf = null,
			$set_session = false) {
		error_reporting(0);

		// $ldap_class = $this->get_ldap_class();
		if ($this->ldap_mode === LDAPModes::OPEN_LDAP) {
			$ldap_class = OpenLDAP::_get_class();
		// } elseif ($this->ldap_mode === LDAPModes::AD_LDAP) {
			// $ldap_class = ADLDAP::_get_class();
		} else {
			throw new NotImplementedException();
		}

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		if ($new_password) {
			if (!preg_match("/.{8,}/", $new_password)) {
				$this->messages[] = new Message(E103, MessageSeverity::ERROR);
				ldap_close($con);
				return false;
			}
	
			if (!preg_match("/[0-9]/", $new_password)) {
				$this->messages[] = new Message(E104, MessageSeverity::ERROR);
				ldap_close($con);
				return false;
			}

			if (!preg_match("/[a-zA-Z]/", $new_password)) {
				$this->messages[] = new Message(E105, MessageSeverity::ERROR);
				ldap_close($con);
				return false;
			}

			if (!preg_match("/[A-Z]/", $new_password)) {
				$this->messages[] = new Message(E106, MessageSeverity::ERROR);
				ldap_close($con);
				return false;
			}

			if (!preg_match("/[a-z]/",$new_password)) {
				$this->messages[] = new Message(E107, MessageSeverity::ERROR);
				ldap_close($con);
				return false;
			}
		}
		/* Test passwords */
		if ($new_password != $new_password_cnf) {
				$this->messages[] = new Message(E108, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		/* Get user to check if it exists */
		$user_search = ldap_search($con, $users_dn, $this->get_search_filter_id($user_id));
		$user_get = ldap_get_entries($con, $user_search);
		if (!$user_get || empty($user_get) || $user_get['count'] === 0) {
			$this->messages[] = new Message("Cannot update user\n".E202, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		$user_dn = $ldap_class::UID."=$user_id,".$users_dn;
		$last_name_upper = strtoupper($last_name);

		/* Form checks */
		$entry = array();
		$entry_add = array();
		$entry_del = array();
		/* Required */
		$entry[$ldap_class::FIRST_NAME] = "$first_name";
		$entry[$ldap_class::LAST_NAME] = "$last_name_upper";

		/* Required but can be missing */
		if ($user_get[0][strtolower($ldap_class::DISPLAY_NAME)]) {
			$entry[$ldap_class::DISPLAY_NAME] = "$first_name $last_name_upper";
		} else {
			$entry_add[$ldap_class::DISPLAY_NAME] = "$first_name $last_name_upper";
		}
		if ($user_get[0][strtolower($ldap_class::MAIL)]) {
			$entry[$ldap_class::MAIL] = "$mail";
		} else {
			$entry_add[$ldap_class::MAIL] = "$mail";
		}

		/* Optionnals */
		if($phone_number) {
			$temp_phone = trim(preg_replace('/(\d{2})/', '\\1 ',
				preg_replace('/\s+/', '',
					preg_replace('/^\+?33/', '0', $phone_number))));
			if ($user_get[0][strtolower($ldap_class::PHONE_NUMBER)]) {
				$entry[$ldap_class::PHONE_NUMBER] = "$temp_phone";
			} else {
				$entry_add[$ldap_class::PHONE_NUMBER] = "$temp_phone";
			}
		} else {
			if ($user_get[0][strtolower($ldap_class::PHONE_NUMBER)]) {
				$entry_del[$ldap_class::PHONE_NUMBER] = array();
			}
		}
		if($mobile_number) {
			$temp_phone = trim(preg_replace('/(\d{2})/', '\\1 ', 
				preg_replace('/\s+/', '',
					preg_replace('/^\+?33/', '0', $mobile_number))));
			if ($user_get[0][strtolower($ldap_class::MOBILE_NUMBER)]) {
				$entry[$ldap_class::MOBILE_NUMBER] = "$temp_phone";
			} else {
				$entry_add[$ldap_class::MOBILE_NUMBER] = "$temp_phone";
			}
		} else {
			if ($user_get[0][strtolower($ldap_class::MOBILE_NUMBER)]) {
				$entry_del[$ldap_class::MOBILE_NUMBER] = array();
			}
		}
		if($job_title) {
			if ($user_get[0][strtolower($ldap_class::TITLE)]) {
				$entry[$ldap_class::TITLE] = "$job_title";
			} else {
				$entry_add[$ldap_class::TITLE] = "$job_title";
			}
		} else {
			if ($user_get[0][strtolower($ldap_class::TITLE)]) {
				$entry_del[$ldap_class::TITLE] = array();
			}
		}
		if($img_content) {
			if ($user_get[0][strtolower($ldap_class::IMAGE)]) {
				$entry[$ldap_class::IMAGE] = "$img_content";
			} else {
				$entry_add[$ldap_class::IMAGE] = "$img_content";
			}
		} else {
			if ($user_get[0][strtolower($ldap_class::IMAGE)]) {
				$entry_del[$ldap_class::IMAGE] = array();
			}
		}

		/* Remove new user flag attribute if exists */
		if ($user_get[0][strtolower($ldap_class::INITIALS)]) {
			$entry_del[$ldap_class::INITIALS] = array();
		}

		/* Encode password if exists */
		if ($new_password) {
			$encoded_new_password = "{SHA}" . base64_encode(pack("H*", sha1($new_password)));
			/* Check if new password is different than old */
			if ($user_get[0][strtolower($ldap_class::PASSWORD)][0] != $encoded_new_password) {
				$entry[$ldap_class::PASSWORD] = "$encoded_new_password";
			}
		}

		/* DEBUG [
		$this->messages[] = new Message("<span>XX: $first_name</span>");
		$this->messages[] = new Message("<span>XX: $last_name_upper</span>");
		$this->messages[] = new Message("<span>XX: $first_name $last_name_upper</span>");
		$this->messages[] = new Message("<span>XX: $mail</span>");
		$this->messages[] = new Message("<span>XX: $phone_number</span>");
		$this->messages[] = new Message("<span>XX: $mobile_number</span>");
		$this->messages[] = new Message("<span>XX: $img_content</span>");
		$this->messages[] = new Message("<span>XX: $job_title</span>");
		$this->messages[] = new Message("<span>XX: $encoded_new_password</span>");
		/* DEBUG ] */


		/* DEBUG [ /* 
		foreach ($entry as $k => $v) {
			$this->messages[] = new Message("Modify :: $k = $v");
		}
		foreach ($entry_add as $k => $v) {
			$this->messages[] = new Message("Add :: $k = $v");
		}
		foreach ($entry_del as $k => $v) {
			$this->messages[] = new Message("Del :: $k = $v");
		}
		/* DEBUG ] */

		$updated = empty($entry) ? true : ldap_modify($con, $user_dn, $entry);
		$inserted = empty($entry_add) ? true : ldap_mod_add($con, $user_dn, $entry_add);
		$deleted = empty($entry_del) ? true : ldap_mod_del($con, $user_dn, $entry_del);

		/* Check modification result */
		if (!$updated || !$inserted || !$deleted) {
			$error = ldap_error($con);
			$errno = ldap_errno($con);
			$this->messages[] = new Message("User cannot be updated, please contact a technical manager",
				MessageSeverity::ERROR);
			$this->messages[] = new Message("$errno - $error", MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		} else {
			$this->messages[] = new Message("The user <strong>$user_id</strong> has been updated",
				MessageSeverity::SUCCESS);

			if ($set_session) {
				$user_search = ldap_search($con, $users_dn, $this->get_search_filter_id($user_id));
				$user_get = ldap_get_entries($con, $user_search);

				$user = new User($user_get[0], $user_dn, $this->ldap_mode);

				User::set_session($user);
			}
		}

		ldap_close($con);
		
		error_reporting(E_ALL);
		return true;
	}

	/**
	 * Update an user entry.
	 * WARNING: Only OpenLDAP is implemented.
	 *
	 * @param $users_dn {string} : The Users DN to apply
	 * @param $groups_dn {string} : The Groups DN to apply
	 * @param $user_id {string} : The uid field
	 * @param $group {string} : The group to add user
	 * @param $first_name {string} : The cn field
	 * @param $last_name {string} : The sn field
	 * @param $mail {string} : The mail field
	 * @return boolean : <code>true</code> if the user has been
	 * created, <code>false</code> otherwise.
	 */
	function add_user($users_dn, $groups_dn, $user_id, $group, $first_name,
		$last_name, $mail) {
		error_reporting(0);

		// $ldap_class = $this->get_ldap_class();
		if ($this->ldap_mode === LDAPModes::OPEN_LDAP) {
			$ldap_class = OpenLDAP::_get_class();
		// } elseif ($this->ldap_mode === LDAPModes::AD_LDAP) {
			// $ldap_class = ADLDAP::_get_class();
		} else {
			throw new NotImplementedException();
		}

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		/* get current user to check connection */
		$curr_uid = $_SESSION[User::UID];
		$admin_search = ldap_search($con, $users_dn, $this->get_search_filter_id($curr_uid));
		$admin_get = ldap_get_entries($con, $admin_search);

		if (!$admin_get) {
			$this->messages[] = new Message(E200, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		/* Check if user exists */
		$user_search = ldap_search($con, $users_dn, $this->get_search_filter_id($user_id));
		$user_get = ldap_get_entries($con, $user_search);

		if ($user_get != null && !empty($user_get) && $user_get['count'] > 0) {
			$this->messages[] = new Message(E201, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		/* Create dn */
		$last_name_upper = strtoupper($last_name);
		$dn_newuser = $ldap_class::UID."=$user_id,".$users_dn;
		
		/* Create default password */
		$encoded_new_password = "{SHA}".base64_encode(pack("H*", sha1('changeit')));

		/* Create user */
		$entry = array();
		$entry["objectClass"][0] = "top";
		$entry["objectClass"][1] = "person";
		$entry["objectClass"][2] = "organizationalPerson";
		$entry["objectClass"][3] = "inetOrgPerson";
		$entry[$ldap_class::PASSWORD] = "$encoded_new_password";
		$entry[$ldap_class::FIRST_NAME] = "$first_name";
		$entry[$ldap_class::LAST_NAME] = "$last_name_upper";
		$entry[$ldap_class::DISPLAY_NAME] = "$first_name $last_name_upper";
		$entry[$ldap_class::MAIL] = "$mail";
		$entry[$ldap_class::UID] = "$user_id";
		if ($ldap_class::INITIALS != null) {
			$entry[$ldap_class::INITIALS] = "new";
		}

		/* Add user to users dn */
		if (ldap_add($con, $dn_newuser, $entry) === false) {
			$error = ldap_error($con);
			$errno = ldap_errno($con);
			$this->messages[] = new Message("User cannot be added, please contact a technical manager",
				MessageSeverity::ERROR);
			$this->messages[] = new Message("$errno - $error", MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		} else {
			$this->messages[] = new Message("The user <strong>$user_id</strong> has been created",
				MessageSeverity::SUCCESS);
		}

		/* Add user to group */
		$dn_newgroup = "cn=$group,".$groups_dn;
		$entry_goup = array();
		$entry_goup["member"] = "$dn_newuser";

		if (ldap_mod_add($con, $dn_newgroup, $entry_goup) === false) {
			$error = ldap_error($con);
			$errno = ldap_errno($con);
			$this->messages[] = new Message("Cannot add user to group <strong>$group</strong>, please contact a technical manager",
				MessageSeverity::ERROR);
			$this->messages[] = new Message("$errno - $error", MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		} else {
			$this->messages[] = new Message("New user <strong>$user_id</strong> has been added to group <strong>$group</strong>\nThe user is <strong>active</strong> and ready to be used",
				MessageSeverity::SUCCESS);
		}
		ldap_close($con);

		error_reporting(E_ALL);
		return true;
	}

	function remove_user_from_group($users_dn, $groups_dn, $user_uid, $group_cn) {
		error_reporting(0);

		$ldap_class = $this->get_ldap_class();

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		/* Get user to check if it exists */
		$user_search = ldap_search($con, $groups_dn, "(member=uid=".strtolower($user_uid).",".$users_dn.")");
		$user_entry = ldap_first_entry($con, $user_search);
		if (!$user_entry || empty($user_entry) || $user_entry['count'] === 0) {
			$this->messages[] = new Message("Cannot remove user from group\n".E202, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		$user_dn = $ldap_class::UID."=$user_uid,".$users_dn;
		$group_dn = "cn=$group_cn,".$groups_dn;

		$entry_del = array();
		$entry_del['member'] = $user_dn;

		$deleted = empty($entry_del) ? true : ldap_mod_del($con, $group_dn, $entry_del);

		/* Check modification result */
		if (!$deleted) {
			$error = ldap_error($con);
			$errno = ldap_errno($con);
			$this->messages[] = new Message("The user <strong>$user_uid</strong> cannot be removed from group <strong>$group_cn</strong>",
				MessageSeverity::ERROR);
			$this->messages[] = new Message("$errno - $error", MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		} else {
			$this->messages[] = new Message("User <strong>$user_uid</strong> has been removed from group <strong>$group_cn</strong>",
				MessageSeverity::SUCCESS);
		}
		ldap_close($con);

		error_reporting(E_ALL);
		return true;
	}

	function remove_user($users_dn, $groups_dn, $user_id, $force = false) {
		error_reporting(0);

		$ldap_class = $this->get_ldap_class();

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		if (!$force && $_SESSION[User::UID] === $user_id) {
			$this->messages[] = new Message("You cannot remove yourself! WTF!?!",
				MessageSeverity::WARNING);
			ldap_close($con);
			return false;
		}

		$removed = false;
		foreach ($this->get_users_with_group($users_dn, $groups_dn) as $user) {
			$curr_uid = $user[$ldap_class::UID][0];
			if ($user_id === $curr_uid) {
				$user_groups = $user['groups_cn'];
				$user_dn = $user['user_dn'];
				$removed = true;
				foreach ($user_groups as $group_cn) {
					$removed = $removed && $this->remove_user_from_group($users_dn, $groups_dn, $user_id, $group_cn);
				}
				break;
			}
		}

		/* Get user to check if it exists */
		$user_search = ldap_search($con, $users_dn, $this->get_search_filter_id($user_id));
		$user_entry = ldap_first_entry($con, $user_search);
		if (!$user_entry || empty($user_entry) || $user_entry['count'] === 0) {
			$this->messages[] = new Message("Cannot remove user\n".E202, MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		}

		if (ldap_delete($con, $user_dn) === false) {
			$error = ldap_error($con);
			$errno = ldap_errno($con);
			$this->messages[] = new Message("Cannot remove user <strong>$user_id</strong>, please contact a technical manager",
				MessageSeverity::ERROR);
			$this->messages[] = new Message("$errno - $error", MessageSeverity::ERROR);
			ldap_close($con);
			return false;
		} else {
			$this->messages[] = new Message("User <strong>$user_id</strong> has been deleted",
				MessageSeverity::SUCCESS);
		}
		ldap_close($con);

		error_reporting(E_ALL);
		return $removed;
	}

	function get_user($users_dn, $user_id) {
		error_reporting(0);

		$ldap_class = $this->get_ldap_class();

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		// /* Check if user exists */
		$user_search = ldap_search($con, $users_dn, $this->get_search_filter_id($user_id));
		$user_get = ldap_get_entries($con, $user_search);
		$user_entry = ldap_first_entry($con, $user_search);
		$user_dn = ldap_get_dn($con, $user_entry);

		$user = new User($user_get[0], $user_dn, $this->ldap_mode);

		ldap_close($con);

		error_reporting(E_ALL);
		return $user;
	}

	function get_user_by_display_name($users_dn, $user_display_name) {
		error_reporting(0);

		$ldap_class = $this->get_ldap_class();

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		$vars = array_filter(explode(' ', preg_replace('/,/', '', $user_display_name)));

		$lastnames = [];
		$firstnames = [];

		foreach ($vars as $var) {
			if (ctype_upper($var)) {
				$lastnames[] = $var;
			} else {
				$firstnames[] = $var;
			}
		}

		$filter = "(&".$this->get_search_filter()."(|(".$ldap_class::DISPLAY_NAME."=$user_display_name)(&(".
			$ldap_class::LAST_NAME."=".join(' ', $lastnames).")(".
			$ldap_class::FIRST_NAME."=".join(' ', $firstnames)."))))";

		// /* Check if user exists */
		$user_search = ldap_search($con, $users_dn, $filter);
		$user_get = ldap_get_entries($con, $user_search);
		$user_entry = ldap_first_entry($con, $user_search);
		$user_dn = ldap_get_dn($con, $user_entry);

		$user = new User($user_get[0], $user_dn, $this->ldap_mode);

		ldap_close($con);

		error_reporting(E_ALL);
		return $user;
	}

	function get_users($users_dn) {
		error_reporting(0);

		$ldap_class = $this->get_ldap_class();

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		/* get current user to check connection */
		$curr_uid = $_SESSION[User::UID];
		$admin_search = ldap_search($con, $users_dn, $this->get_search_filter_id($curr_uid));
		$admin_get = ldap_get_entries($con, $admin_search);

		if (!$admin_get) {
			$this->messages[] = new Message(E200, MessageSeverity::ERROR);
			utils\Messages\store_messages($this->messages);
			ldap_close($con);
			return null;
		}

		/* Get all users */
		$users_search = ldap_search($con, $users_dn, "(&".$this->get_search_filter()."(".$ldap_class::UID."=*))");
		ldap_sort($con, $users_search, $ldap_class::UID);
		$users_get = ldap_get_entries($con, $users_search);

		$users = array();
		foreach ($users_get as $user) {
			$user_uid = $user[$ldap_class::UID][0];
			if (!empty($user_uid)) {
				$users[] = $user;
			}
		}
		ldap_close($con);

		error_reporting(E_ALL);
		return $users;
	}

	function get_users_with_group($users_dn, $groups_dn) {
		error_reporting(0);

		$ldap_class = $this->get_ldap_class();

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		$users_get = $this->get_users($users_dn);

		$users = array();
		foreach ($users_get as $user) {
			$user_uid = $user[$ldap_class::UID][0];
			if (!empty($user_uid)) {
				$user_search = ldap_search($con, $users_dn, $this->get_search_filter_id($user_uid));
				$user_entry = ldap_first_entry($con, $user_search);
				$user_dn = ldap_get_dn($con, $user_entry);

				$user_groups = array();
				$groups = $this->get_groups($groups_dn);

				foreach ($groups as $group) {
					$cn = trim($group["cn"][0]);
					if (!empty($cn)) {
						$group_search = ldap_search($con, $groups_dn, "(cn=$cn)");
						$group_entry = ldap_first_entry($con, $group_search);
						$group_dn = ldap_get_dn($con, $group_entry);
						if ($this->is_in_group($user_dn, $group_dn)) {
							$user_groups[] = $cn;
						}
					}
				}
				$user['user_dn'] = $user_dn;
				$user['groups_cn'] = $user_groups;
				$users[] = $user;
			}
		}
		ldap_close($con);

		error_reporting(E_ALL);
		return $users;
	}

	function get_groups($groups_dn) {
		error_reporting(0);

		$con = ldap_connect($this->ldap_server, $this->ldap_port);
		ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

		/* Find groups */
		$groups_search_filter = "(objectClass=groupOfNames)";
		$groups_search = ldap_search($con, $groups_dn, $groups_search_filter);
		$groups_get = ldap_get_entries($con, $groups_search);

		$groups = array();
		foreach ($groups_get as $group) {
			$cn = trim($group["cn"][0]);
			if (!empty($cn)) {
				$groups[] = $group;
			}
		}
		ldap_close($con);

		error_reporting(E_ALL);
		return $groups;
	}

}
?>