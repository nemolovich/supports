<?php
namespace utils\LDAP;

class OpenLDAP {

	const UID = 'uid';
	const SEARCH_IDS = [self::UID, self::MAIL];
	const SEARCH_FILTERS = ['objectClass=organizationalPerson',
							'objectClass=inetOrgPerson',
							'objectClass=top',
							'objectClass=person'];
	const FIRST_NAME = 'cn';
	const LAST_NAME = 'sn';
	const DISPLAY_NAME = 'displayName';
	const MAIL = 'mail';
	const PHONE_NUMBER = 'telephoneNumber';
	const MOBILE_NUMBER = 'mobile';
	const TITLE = 'title';
	const IMAGE = 'jpegPhoto';
	const INITIALS = 'initials';
	const PASSWORD = 'userPassword';

	function __construct() {
		throw new NotImplementedException();
	}

	static function _get_class() {
		return get_class();
	}

}
?>
