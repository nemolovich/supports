<?php
namespace utils\LDAP;

class User
{
	public $user_dn;
	public $uid;
	public $first_name;
	public $last_name;
	public $display_name;
	public $mail;
	public $phone_number;
	public $mobile_number;
	public $title;
	public $image;
	public $initials;

	private static $CACHE_DIR = 'cache';
	private static $APP_CONTEXT = '/';
	const BASE_64_ROOT = "data:image/png;base64,";

	const USER = 'LDAP_USER';
	const DN = 'USER_DN';
	const UID = 'USER_UID';
	const FIRST_NAME = 'USER_FIRST_NAME';
	const LAST_NAME = 'USER_LAST_NAME';
	const DISPLAY_NAME = 'USER_DISPLAY_NAME';
	const MAIL = 'USER_MAIL';
	const PHONE_NUMBER = 'USER_PHONE_NUMBER';
	const MOBILE_NUMBER = 'USER_MOBILE_NUMBER';
	const TITLE = 'USER_TITLE';
	const IMAGE = 'USER_IMAGE';
	const IMAGE_CACHED = 'USER_IMAGE_CACHED';
	const INITIALS = 'USER_INITIALS';

	function __construct($entry, $user_dn, $mode = LDAPModes::__default) {
		$this->user_dn = $user_dn;

		$OBJ = null;
		if ($mode === LDAPModes::OPEN_LDAP) {
			$OBJ = OpenLDAP::_get_class();
		} elseif ($mode === LDAPModes::AD_LDAP) {
			$OBJ = ADLDAP::_get_class();
		} else {
			throw new NotImplementedException();
		}

		$user_uid = $entry[strtolower($OBJ::UID)][0];
		$this->uid = $user_uid;
		$this->first_name = $entry[strtolower($OBJ::FIRST_NAME)][0];
		$this->last_name = $entry[strtolower($OBJ::LAST_NAME)][0];
		$this->display_name = $entry[strtolower($OBJ::DISPLAY_NAME)][0];
		$this->mail = $entry[strtolower($OBJ::MAIL)][0];
		$this->phone_number = $entry[strtolower($OBJ::PHONE_NUMBER)][0];
		$this->mobile_number = $entry[strtolower($OBJ::MOBILE_NUMBER)][0];
		$this->title = $entry[strtolower($OBJ::TITLE)][0];
		$img = $entry[strtolower($OBJ::IMAGE)][0];
		$image_cached = $entry[strtolower(self::IMAGE_CACHED)];
		if (($image_cached || !$img) && file_exists(self::get_image_path($user_uid))) {
			$this->image = (self::$APP_CONTEXT === '/' ? '' : self::$APP_CONTEXT).'/'.self::get_image_path($user_uid);
		}elseif ($img) {
			$this->image = self::BASE_64_ROOT.base64_encode($img);
		} else {
			$this->image = null;
		}
		if ($OBJ::INITIALS !== null) {
			$this->initials = $entry[strtolower($OBJ::INITIALS)][0];
		}
	}

	static function set_app_context($context) {
		self::$APP_CONTEXT = $context;
	}

	static function set_cache_directory($directory) {
		self::$CACHE_DIR = $directory;
	}

	static function build($session, $mode = LDAPModes::__default) {
		$params = Array();
		$user_dn = $session[self::DN];

		$OBJ = null;
		if ($mode === LDAPModes::OPEN_LDAP) {
			$OBJ = OpenLDAP::_get_class();
		} elseif ($mode === LDAPModes::AD_LDAP) {
			$OBJ = ADLDAP::_get_class();
		} else {
			throw new NotImplementedException();
		}

		$params[strtolower($OBJ::UID)]           = [$session[self::UID]];
		$params[strtolower($OBJ::FIRST_NAME)]    = [$session[self::FIRST_NAME]];
		$params[strtolower($OBJ::LAST_NAME)]     = [$session[self::LAST_NAME]];
		$params[strtolower($OBJ::DISPLAY_NAME)]  = [$session[self::DISPLAY_NAME]];
		$params[strtolower($OBJ::MAIL)]          = [$session[self::MAIL]];
		$params[strtolower($OBJ::PHONE_NUMBER)]  = [$session[self::PHONE_NUMBER]];
		$params[strtolower($OBJ::MOBILE_NUMBER)] = [$session[self::MOBILE_NUMBER]];
		$params[strtolower($OBJ::TITLE)]         = [$session[self::TITLE]];
		$params[strtolower(self::IMAGE_CACHED)]  = true;
		$img = $session[self::IMAGE];
		if ($img && self::is_base64_image($img)) {
			$img = base64_decode(self::trunc_data_header($img));
		}
		$params[strtolower($OBJ::IMAGE)]         = [$img];
		if ($OBJ::INITIALS !== null) {
			$params[strtolower($OBJ::INITIALS)]  = [$session[self::INITIALS]];
		}
		return new User($params, $user_dn, $mode);
	}

	private static function trunc_data_header($str) {
		return preg_replace('/^'.preg_quote(self::BASE_64_ROOT, '/').'/', '', $str);
	}

	private static function get_image_path($user_uid) {
		return self::$CACHE_DIR.'/'.$user_uid.'.jpg';
	}

	private static function is_base64_image($img) {
		return 0 === strpos($img, self::BASE_64_ROOT);
	}

	static function set_session($user) {
		$_SESSION[self::USER] = serialize($user);
		$_SESSION[self::DN] = $user->user_dn;
		$_SESSION[self::UID] = $user->uid;
		$_SESSION[self::FIRST_NAME] = $user->first_name;
		$_SESSION[self::LAST_NAME] = $user->last_name;
		$_SESSION[self::DISPLAY_NAME] = $user->display_name;
		$_SESSION[self::MAIL] = $user->mail;
		$_SESSION[self::PHONE_NUMBER] = $user->phone_number;
		$_SESSION[self::MOBILE_NUMBER] = $user->mobile_number;
		$_SESSION[self::TITLE] = $user->title;
		$_SESSION[self::IMAGE] = $user->image;
		$_SESSION[self::IMAGE_CACHED] = !!($user->image && !self::is_base64_image($user->image));
		$_SESSION[self::INITIALS] = $user->initials;
	}
}
?>
