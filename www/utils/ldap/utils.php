<?php
	namespace utils\LDAP;

	require 'LDAPConnection.php';
	// use utils\Messages\Message as Message;
	// use utils\Messages\MessageSeverity as MessageSeverity;
	// use const utils\Messages\MSG_ATTR;
	// use function utils\Messages\get_messages;
	// use function utils\Messages\has_next;
	// use function utils\Messages\next_message;
	// use function utils\Messages\store_messages;

	const E001 = 'Error E001 - Cannot connect to LDAP server';
	const E101 = "Error E101 - Incorrect user or password";
	const E103 = "Error E103 - Password must contains at least 8 characters";
	const E104 = "Error E104 - Password must contains at least one number";
	const E105 = "Error E105 - Password must contains at least one letter";
	const E106 = "Error E106 - Password must contains at least on upper character";
	const E107 = "Error E107 - Password must contains at least one lower character";
	const E108 = "Error E108 - Password verification failed";
	const E200 = "Error E200 - Cannot request on server";
	const E201 = "Error E201 - User already exists";
	const E202 = "Error E202 - User does not exist";

?>
