<?php
namespace utils\Messages;

use utils\Messages\MessageSeverity as MessageSeverity;

class Message
{
	public $msg;
	public $severity;
	
	function __construct($msg, $severity = MessageSeverity::__default) {
		$this->msg = $msg;
		$this->severity = $severity;
	}
}
?>
