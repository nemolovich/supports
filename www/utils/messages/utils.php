<?php
	namespace utils\Messages;

	require 'Message.php';
	require 'MessagesList.php';
	require 'MessageSeverity.php';
	use utils\Messages\Message as Message;
	use utils\Messages\MessagesList as MessagesList;
	use utils\Messages\Message as MessageSeverity;

	const MSG_ATTR = 'SESSION_MESSAGES';

	function get_messages() {
		$list = unserialize($_SESSION[MSG_ATTR]);
		return ($list && sizeof($list) > 0) ? $list->messages : [];
	}

	function store_messages($messages) {
		$_SESSION[MSG_ATTR] = serialize(new MessagesList($messages));
	}

	function next_message(&$messages) {
		$message = null;
		if (has_next($messages)) {
			$message = array_shift($messages);
		}
		return $message;
	}

	function has_next(&$messages) {
		return ($messages != null) && sizeof($messages) > 0;
	}

	function clone_message($message) {
		return new Message($message->msg, $message->severity);
	}

?>
